<?php

class Index extends Controller
{
	function __construct()
	{
		parent::__construct();
		Session::init();
		$logged = Session::get('loggedIn');

		if($logged == false){
			Session::destroy();
			header('location: ../Web/login');
			exit;
		}
	}

	function index()
	{
		$this->view->render('home/home');
	}

	function run_teams()
	{
		$this->model->run_teams();
	}

	function logout(){
		
		Session::destroy();
		header('location: ../login');
		exit;
	}
}