CREATE OR REPLACE FUNCTION chance_to_win (p_id_team_1 teams.team_name%TYPE, p_id_team_2 teams.team_name%TYPE) RETURN VARCHAR2 IS
  v_result VARCHAR2(500);
  v_no_stars_team_1 teams.no_stars%TYPE;
  v_no_stars_team_2 teams.no_stars%TYPE;
  v_team_1_name teams.team_name%TYPE;
  v_team_2_name teams.team_name%TYPE;
  v_worth_team_1 teams.worth%TYPE;
  v_worth_team_2 teams.worth%TYPE;
  v_procent_team_1_stars NUMBER(5,2);
  v_procent_team_2_stars NUMBER(5,2);
  v_procent_team_1_worth NUMBER(5,2);
  v_procent_team_2_worth NUMBER(5,2);
  v_procent_team_1_final NUMBER(5,0);
  v_procent_team_2_final NUMBER(5,0);
  v_procent_draw NUMBER(5,2);
  BEGIN
    SELECT no_stars, team_name, worth INTO v_no_stars_team_1, v_team_1_name, v_worth_team_1 FROM teams WHERE team_name = p_id_team_1;
    SELECT no_stars, team_name, worth INTO v_no_stars_team_2, v_team_2_name, v_worth_team_2 FROM teams WHERE team_name = p_id_team_2;
    v_procent_team_1_stars := 10 + 100 * (v_no_stars_team_1 / (v_no_stars_team_1 + v_no_stars_team_2)) * (v_no_stars_team_1 / (v_no_stars_team_1 + v_no_stars_team_2));
    v_procent_team_2_stars := 10 + 100 * (v_no_stars_team_2 / (v_no_stars_team_1 + v_no_stars_team_2)) * (v_no_stars_team_2 / (v_no_stars_team_1 + v_no_stars_team_2));
    
    v_procent_team_1_worth := 10 + 100 * (v_worth_team_1 / (v_worth_team_1 + v_worth_team_2)) * (v_worth_team_1 / (v_worth_team_1 + v_worth_team_2));
    v_procent_team_2_worth := 10 + 100 * (v_worth_team_2 / (v_worth_team_1 + v_worth_team_2)) * (v_worth_team_2 / (v_worth_team_1 + v_worth_team_2));
    
    v_procent_team_1_final := (v_procent_team_1_stars * 6 + v_procent_team_1_worth) / 7;
    v_procent_team_2_final := (v_procent_team_2_stars * 6 + v_procent_team_2_worth) / 7;
    v_procent_draw := 100 - v_procent_team_1_final - v_procent_team_2_final;
    v_procent_team_1_final := v_procent_team_1_final + v_procent_draw/2;
    v_procent_team_2_final := 100 - v_procent_team_1_final;
    
    v_result := v_procent_team_1_final || '-' || v_procent_team_2_final;
    RETURN v_result;
  END chance_to_win;
/


CREATE OR REPLACE PROCEDURE disable_constraints IS
  CURSOR turn_off IS SELECT 'ALTER TABLE '||table_name||' DISABLE CONSTRAINT '||constraint_name
                    FROM user_constraints
                    WHERE constraint_type = 'R';
  to_execute VARCHAR2(100);
  BEGIN
    OPEN turn_off;
    LOOP
      FETCH turn_off INTO to_execute;
      EXIT WHEN turn_off%NOTFOUND;
      EXECUTE IMMEDIATE to_execute;
    END LOOP;
  END;
/


CREATE OR REPLACE PROCEDURE enable_constraints IS
  CURSOR turn_on IS SELECT 'ALTER TABLE '||table_name||' ENABLE CONSTRAINT '||constraint_name
                    FROM user_constraints
                    WHERE constraint_type = 'R';
  to_execute VARCHAR2(100);
  BEGIN
    OPEN turn_on;
    LOOP
      FETCH turn_on INTO to_execute;
      EXIT WHEN turn_on%NOTFOUND;
      EXECUTE IMMEDIATE to_execute;
    END LOOP;
  END;
/