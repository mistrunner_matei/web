ALTER TABLE Match_status DROP CONSTRAINT match_fk_Match_status;
ALTER TABLE Goals DROP CONSTRAINT player_fk_Goals;
ALTER TABLE Goals DROP CONSTRAINT match_fk_Goals;
ALTER TABLE Goals DROP CONSTRAINT team_fk_Goals;
ALTER TABLE Matches DROP CONSTRAINT competition_fk_Matches;
ALTER TABLE Matches DROP CONSTRAINT home_team_fk_Matches;
ALTER TABLE Matches DROP CONSTRAINT away_team_fk_Matches;
ALTER TABLE Matches DROP CONSTRAINT stadium_fk_Matches;
ALTER TABLE Players DROP CONSTRAINT team_fk_Players;
ALTER TABLE Teams DROP CONSTRAINT region_fk_Teams;
ALTER TABLE Teams DROP CONSTRAINT group_fk_Teams;
ALTER TABLE Teams DROP CONSTRAINT coach_fk_Teams;
ALTER TABLE Competitions DROP CONSTRAINT region_fk_Competitions;
ALTER TABLE Team_status DROP CONSTRAINT team_fk_Team_status;
ALTER TABLE Coaches DROP CONSTRAINT team_fk_Coaches;
ALTER TABLE Groups DROP CONSTRAINT competition_fk_Groups;

DROP TABLE Stadiums;
DROP TABLE Match_status;
DROP TABLE Goals;
DROP TABLE Matches;
DROP TABLE Players;
DROP TABLE Teams;
DROP TABLE Regions;
DROP TABLE Competitions;
DROP TABLE Team_status;
DROP TABLE Coaches;
DROP TABLE Groups;
DROP TABLE Users;
Drop TABLE Questions;
/

CREATE TABLE Users (
  Username VARCHAR2(20) NOT NULL,
  Encrypted_password VARCHAR2(100) NOT NULL,
  No_total NUMBER(5,0),
  No_correct NUMBER(5,0)
);
/

CREATE TABLE Stadiums (
  ID_stadium VARCHAR2(10) NOT NULL,
  Stadium_Name VARCHAR2(50),
  CONSTRAINT stadium_pk PRIMARY KEY (ID_stadium)
);
/

CREATE TABLE Match_status (
  ID_match VARCHAR2(10) NOT NULL,
  Home_team_possession NUMBER(3,0),
  Away_team_possession NUMBER(3,0),
  No_yellow_cards_home_team NUMBER(2,0),
  No_yellow_cards_away_team NUMBER(2,0),
  No_red_cards_home_team NUMBER(2,0),
  No_red_cards_away_team NUMBER(2,0),
  No_corners_home_team NUMBER(2,0),
  No_corners_away_team NUMBER(2,0)
);
/

CREATE TABLE Goals (
  ID_player VARCHAR2(10) NOT NULL,
  ID_match VARCHAR2(10) NOT NULL,
  ID_team VARCHAR2(10) NOT NULL,
  Minute_of_goal VARCHAR2(5),
  Distance NUMBER(2,0),
  Own_goal NUMBER(1)
);
/

CREATE TABLE Matches (
  ID_match VARCHAR2(10) NOT NULL,
  ID_competition VARCHAR2(10) NOT NULL,
  ID_home_team VARCHAR2(10) NOT NULL,
  ID_away_team VARCHAR2(10) NOT NULL,
  ID_stadium VARCHAR2(10) NOT NULL,
  Start_time DATE,
  Score VARCHAR2(5),
  CONSTRAINT match_pk PRIMARY KEY (ID_match)
);
/

CREATE TABLE Players (
  ID_player VARCHAR2(10) NOT NULL,
  ID_team VARCHAR2(10) NOT NULL,
  First_name VARCHAR2(50),
  Last_name VARCHAR2(50),
  Shirt_number NUMBER(2,0),
  Date_of_birth DATE,
  Nationality VARCHAR2(50),
  Pos VARCHAR2(50),
  Lineup NUMBER(1),
  CONSTRAINT player_pk PRIMARY KEY (ID_player)
);
/

CREATE TABLE Teams (
  ID_team VARCHAR2(10) NOT NULL,
  ID_region VARCHAR2(10) NOT NULL,
  ID_group VARCHAR2(10) NOT NULL,
  ID_coach VARCHAR2(10) NOT NULL,
  Team_name VARCHAR2(50),
  Formation VARCHAR2(20),
  Worth NUMBER(10,0),
  No_stars NUMBER(2,0),
  CONSTRAINT team_pk PRIMARY KEY (ID_team)
);
/

CREATE TABLE Regions (
  ID_region VARCHAR2(10) NOT NULL,
  Region_name VARCHAR2(50),
  CONSTRAINT region_pk PRIMARY KEY (ID_region)
);
/

CREATE TABLE Competitions (
  ID_competition VARCHAR2(10) NOT NULL,
  ID_region VARCHAR2(10) NOT NULL,
  Competition_name VARCHAR2(50),
  CONSTRAINT competition_pk PRIMARY KEY (ID_competition)
);
/

CREATE TABLE Team_status (
  ID_team VARCHAR2(10) NOT NULL,
  No_matches_played NUMBER(3,0),
  No_wins NUMBER(3,0),
  No_draws NUMBER(3,0),
  No_losses NUMBER(3,0),
  No_goals_scored NUMBER(3,0),
  No_goals_received NUMBER(3,0),
  No_points NUMBER(3,0)
);
/

CREATE TABLE Coaches (
  ID_coach VARCHAR2(10) NOT NULL,
  ID_team VARCHAR2(10) NOT NULL,
  First_name VARCHAR2(50),
  Last_name VARCHAR2(50),
  Date_of_birth DATE,
  CONSTRAINT coach_pk PRIMARY KEY (ID_coach)  
);
/

CREATE TABLE Groups (
  ID_group VARCHAR2(10) NOT NULL,
  ID_competition VARCHAR2(10) NOT NULL,
  Group_name VARCHAR2(50),
  No_teams NUMBER(2),
  CONSTRAINT group_pk PRIMARY KEY (ID_group)
);
/
CREATE TABLE Questions(
  
  id_question NUMBER (2),
  question_q VARCHAR2(500),
  PRIMARY KEY(id_question)
 );
 /

 INSERT INTO Questions VALUES (1,'Cine va obtine victoria?');
 INSERT INTO Questions VALUES (2,'Cine va castiga prima repriza?');
 INSERT INTO Questions VALUES (3,'Cine va castiga a doua repriza?');
 INSERT INTO Questions VALUES (4,'Vor marca ambele echipe?');
 INSERT INTO Questions VALUES (5,'Vor marca ambele echipe in prima repriza?');
 INSERT INTO Questions VALUES (6,'Vor marca ambele echipe in a doua repriza?');
 INSERT INTO Questions VALUES (7,'Va marca echipa gazda?');
 INSERT INTO Questions VALUES (8,'Va marca echipa oaspete?');
 INSERT INTO Questions VALUES (9,'Care va fi numarul total de goluri?');
 INSERT INTO Questions VALUES (10,'Care va fi numarul total de goluri inscris de oaspeti?');
 INSERT INTO Questions VALUES (11,'Care va fi numarul total de goluri inscris de gazde?');
 INSERT INTO Questions VALUES (12,'Care va fi numarul total de goluri in prima repriza?');
 INSERT INTO Questions VALUES (13,'Care va fi numarul total de goluri in a doua repriza?');
 INSERT INTO Questions VALUES (14,'Cate cartonase galbene va primi echipa gazda?');
 INSERT INTO Questions VALUES (15,'Cate cartonase galbene va primi echipa oaspete?');
 INSERT INTO Questions VALUES (16,'Cate cartonase rosii va primi echipa gazda?');
 INSERT INTO Questions VALUES (17,'Cate cartonase rosii va primi echipa oaspete?');
 INSERT INTO Questions VALUES (18,'Cate cartonase galbene vor fi in meci?');
 INSERT INTO Questions VALUES (19,'Cate cartonase rosii vor fi in meci?');
 INSERT INTO Questions VALUES (20,'Cine va primi mai multe cartonase galbene?');
 INSERT INTO Questions VALUES (21,'Cine va primi mai multe cartonase rosii?');
 INSERT INTO Questions VALUES (22,'Cate goluri va marca echipa gazda?');
 INSERT INTO Questions VALUES (23,'Cate goluri va marca echipa oaspete?');
 INSERT INTO Questions VALUES (24,'Cate goluri va marca echipa gazda in prima repriza?');
 INSERT INTO Questions VALUES (25,'Cate goluri va marca echipa oaspete in prima repriza?');
 INSERT INTO Questions VALUES (26,'Cate goluri va marca echipa gazda in a doua repriza?');
 INSERT INTO Questions VALUES (27,'Cate goluri va marca echipa oaspete in a doua repriza?');
 INSERT INTO Questions VALUES (28,'Se va marca in prima repriza?');
 INSERT INTO Questions VALUES (29,'Se va marca in a doua repriza?');
 INSERT INTO Questions VALUES (30,'Cate goluri se vor marca in meci?');
 INSERT INTO Questions VALUES (31,'Va fi penalty in meci?');
 INSERT INTO Questions VALUES (32,'Echipa gazda va avea penalty?');
 INSERT INTO Questions VALUES (33,'Echipa oaspete va avea penalty');
 INSERT INTO Questions VALUES (34,'Va fi penalty in prima repriza?');
 INSERT INTO Questions VALUES (35,'Va fi penalty in a doua repriza?');
 INSERT INTO Questions VALUES (36,'Cine nu va inscrie in meci?');
 INSERT INTO Questions VALUES (37,'Care va fi numarul total de cornere in meci?');
 INSERT INTO Questions VALUES (38,'Cate cornere va avea echipa gazda?');
 INSERT INTO Questions VALUES (39,'Cate cornere va avea echipa oaspete?');
 INSERT INTO Questions VALUES (40,'Cine va avea mai multe cornere?');
 
/ 


INSERT INTO USERS VALUES ('Administrator', '$2y$10$biFNqPYNum2jcGxxwACRGO7YNtXZ1fc/ZpKU3H5VglDh9GpsJaUem', 0, 0);

INSERT INTO COACHES VALUES ('C1000', 'T1000', 'Oscar', 'Tabarez', TO_DATE('03-03-1947','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1001', 'T1001', 'Javier', 'Aguirre', TO_DATE('01-12-1958','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1002', 'T1002', 'Carlos', 'Alberto', TO_DATE('17-07-1944','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1003', 'T1003', 'Raymond', 'Domenech', TO_DATE('24-01-1952','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1004', 'T1004', 'Diego', 'Maradona', TO_DATE('30-10-1960','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1005', 'T1005', 'Huh', 'Jung-Moo', TO_DATE('13-02-1955','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1006', 'T1006', 'Otto', 'Rehhagel', TO_DATE('09-08-1938','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1007', 'T1007', 'Lars', 'Lagerback', TO_DATE('16-07-1948','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1008', 'T1008', 'Bob', 'Bradley', TO_DATE('03-03-1958','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1009', 'T1009', 'Fabio', 'Capello', TO_DATE('18-07-1946','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1010', 'T1010', 'Matjaz', 'Kek', TO_DATE('09-09-1961','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1011', 'T1011', 'Rabah', 'Saadane', TO_DATE('03-05-1946','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1012', 'T1012', 'Joachim', 'Low', TO_DATE('03-02-1960','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1013', 'T1013', 'Milovan', 'Rajevac', TO_DATE('02-01-1954','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1014', 'T1014', 'Pim', 'Verbeek', TO_DATE('12-03-1956','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1015', 'T1015', 'Radomir', 'Antic', TO_DATE('22-11-1948','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1016', 'T1016', 'Bertvan', 'Marwijk', TO_DATE('19-05-1952','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1017', 'T1017', 'Takeshi', 'Okada', TO_DATE('25-09-1956','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1018', 'T1018', 'Morten', 'Olsen', TO_DATE('14-09-1949','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1019', 'T1019', 'Paul-Le', 'Guen', TO_DATE('01-03-1964','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1020', 'T1020', 'Gerardo', 'Martino', TO_DATE('20-10-1962','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1021', 'T1021', 'Vladimir', 'Weiss', TO_DATE('22-09-1964','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1022', 'T1022', 'Ricki', 'Herbert', TO_DATE('10-04-1961','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1023', 'T1023', 'Marcelo', 'Lippi', TO_DATE('12-04-1948','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1024', 'T1024', 'Carlos', 'Caetano', TO_DATE('31-10-1963','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1025', 'T1025', 'Carlos', 'Queiroz', TO_DATE('01-03-1953','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1026', 'T1026', 'Sven-Goran', 'Eriksson', TO_DATE('05-02-1948','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1027', 'T1027', 'Kim', 'Jong-Hun', TO_DATE('01-09-1956','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1028', 'T1028', 'Vicente', 'Del-Bosque', TO_DATE('23-12-1950','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1029', 'T1029', 'Marcelo', 'Bielsa', TO_DATE('21-07-1955','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1030', 'T1030', 'Ottmar', 'Hitzfeld', TO_DATE('12-01-1949','DD-MM-YYYY')); 
INSERT INTO COACHES VALUES ('C1031', 'T1031', 'Reinaldo', 'Rueda', TO_DATE('16-04-1957','DD-MM-YYYY')); 

INSERT INTO COMPETITIONS VALUES ('C100', 'R1006', 'FIFA Word Cup 2010');

INSERT INTO GROUPS VALUES ('G1000', 'C100', 'GroupA', 4);
INSERT INTO GROUPS VALUES ('G1001', 'C100', 'GroupB', 4);
INSERT INTO GROUPS VALUES ('G1002', 'C100', 'GroupC', 4);
INSERT INTO GROUPS VALUES ('G1003', 'C100', 'GroupD', 4);
INSERT INTO GROUPS VALUES ('G1004', 'C100', 'GroupE', 4);
INSERT INTO GROUPS VALUES ('G1005', 'C100', 'GroupF', 4);
INSERT INTO GROUPS VALUES ('G1006', 'C100', 'GroupG', 4);
INSERT INTO GROUPS VALUES ('G1007', 'C100', 'GroupH', 4);

INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1000', 'T1002', 'GK', 'Moeneeb', 'Josephs', TO_DATE('1980-05-19', 'YYYY-MM-DD'), 17, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1001', 'T1002', 'DF', 'Siboniso', 'Gaxa', TO_DATE('1984-04-06', 'YYYY-MM-DD'), 37, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1002', 'T1002', 'DF', 'Tsepo', 'Masilela', TO_DATE('1985-05-05', 'YYYY-MM-DD'), 31, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1003', 'T1002', 'DF', 'Aaron', 'Mokoena', TO_DATE('1980-11-25', 'YYYY-MM-DD'), 99, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1004', 'T1002', 'DF', 'Anele', 'Ngcongca', TO_DATE('1987-10-20', 'YYYY-MM-DD'), 20, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1005', 'T1002', 'MF', 'MacBeth', 'Sibaya', TO_DATE('1977-11-25', 'YYYY-MM-DD'), 25, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1006', 'T1002', 'MF', 'Lance', 'Davids', TO_DATE('1985-04-11', 'YYYY-MM-DD'), 11, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1007', 'T1002', 'MF', 'Siphiwe', 'Tshabalala', TO_DATE('1984-09-25', 'YYYY-MM-DD'), 48, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1008', 'T1002', 'FW', 'Katlego', 'Mphela', TO_DATE('1984-11-29', 'YYYY-MM-DD'), 32, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1009', 'T1002', 'MF', 'Steven', 'Pienaar', TO_DATE('1982-03-17', 'YYYY-MM-DD'), 50, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1010', 'T1002', 'MF', 'Teko', 'Modise', TO_DATE('1982-12-22', 'YYYY-MM-DD'), 52, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1011', 'T1002', 'MF', 'Reneilwe', 'Letsholonyane', TO_DATE('1982-06-09', 'YYYY-MM-DD'), 13, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1012', 'T1002', 'MF', 'Kagisho', 'Dikgacoi', TO_DATE('1984-11-24', 'YYYY-MM-DD'), 37, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1013', 'T1002', 'DF', 'Matthew', 'Booth', TO_DATE('1977-03-14', 'YYYY-MM-DD'), 27, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1014', 'T1002', 'DF', 'Lucas', 'Thwala', TO_DATE('1981-10-19', 'YYYY-MM-DD'), 24, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1015', 'T1002', 'GK', 'Itumeleng', 'Khune', TO_DATE('1987-06-20', 'YYYY-MM-DD'), 27, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1016', 'T1002', 'FW', 'Bernard', 'Parker', TO_DATE('1986-03-16', 'YYYY-MM-DD'), 28, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1017', 'T1002', 'FW', 'Siyabonga', 'Nomvethe', TO_DATE('1977-12-02', 'YYYY-MM-DD'), 76, 'South Africa', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1018', 'T1002', 'MF', 'Surprise', 'Moriri', TO_DATE('1980-03-20', 'YYYY-MM-DD'), 34, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1019', 'T1002', 'DF', 'Bongani', 'Khumalo', TO_DATE('1987-01-06', 'YYYY-MM-DD'), 14, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1020', 'T1002', 'DF', 'Siyabonga', 'Sangweni', TO_DATE('1981-09-29', 'YYYY-MM-DD'), 8, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1021', 'T1002', 'GK', 'Shu-Aib', 'Walters', TO_DATE('1981-12-26', 'YYYY-MM-DD'), 26, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1022', 'T1002', 'MF', 'Thanduyise', 'Khuboni', TO_DATE('1986-05-23', 'YYYY-MM-DD'), 9, 'South Africa', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1023', 'T1001', 'GK', '�scar', 'P�rez', TO_DATE('1973-02-01', 'YYYY-MM-DD'), 52, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1024', 'T1001', 'DF', 'Francisco', 'Rodr�guez', TO_DATE('1981-10-20', 'YYYY-MM-DD'), 8, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1025', 'T1001', 'DF', 'Carlos', 'Salcido', TO_DATE('1980-04-02', 'YYYY-MM-DD'), 73, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1026', 'T1001', 'DF', 'Rafael', 'M�rquez', TO_DATE('1979-02-13', 'YYYY-MM-DD'), 91, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1027', 'T1001', 'DF', 'Ricardo', 'Osorio', TO_DATE('1980-03-30', 'YYYY-MM-DD'), 76, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1028', 'T1001', 'MF', 'Gerardo', 'Torrado', TO_DATE('1979-04-30', 'YYYY-MM-DD'), 14, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1029', 'T1001', 'MF', 'Pablo', 'Barrera', TO_DATE('1987-06-21', 'YYYY-MM-DD'), 22, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1030', 'T1001', 'MF', 'Israel', 'Castro', TO_DATE('1980-12-29', 'YYYY-MM-DD'), 31, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1031', 'T1001', 'FW', 'Guillermo', 'Franco', TO_DATE('1976-11-03', 'YYYY-MM-DD'), 21, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1032', 'T1001', 'FW', 'Cuauht�moc', 'Blanco', TO_DATE('1973-01-17', 'YYYY-MM-DD'), 15, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1033', 'T1001', 'FW', 'Carlos', 'Vela', TO_DATE('1989-03-01', 'YYYY-MM-DD'), 28, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1034', 'T1001', 'DF', 'Paul', 'Aguilar', TO_DATE('1986-03-06', 'YYYY-MM-DD'), 10, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1035', 'T1001', 'GK', 'Guillermo', 'Ochoa', TO_DATE('1985-07-13', 'YYYY-MM-DD'), 37, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1036', 'T1001', 'FW', 'Javier', 'Hern�ndez', TO_DATE('1988-06-01', 'YYYY-MM-DD'), 12, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1037', 'T1001', 'DF', 'H�ctor', 'Moreno', TO_DATE('1988-01-17', 'YYYY-MM-DD'), 10, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1038', 'T1001', 'DF', 'Efra�n', 'Ju�rez', TO_DATE('1988-02-22', 'YYYY-MM-DD'), 19, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1039', 'T1001', 'FW', 'Giovani', 'dos-Santos', TO_DATE('1989-05-11', 'YYYY-MM-DD'), 26, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1040', 'T1001', 'MF', 'Andr�s', 'Guardado', TO_DATE('1986-09-28', 'YYYY-MM-DD'), 56, 'Mexico', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1041', 'T1001', 'DF', 'Jonny', 'Magall�n', TO_DATE('1981-11-21', 'YYYY-MM-DD'), 52, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1042', 'T1001', 'DF', 'Jorge', 'Torres-Nilo', TO_DATE('1988-01-16', 'YYYY-MM-DD'), 8, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1043', 'T1001', 'FW', 'Adolfo', 'Bautista', TO_DATE('1979-05-15', 'YYYY-MM-DD'), 37, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1044', 'T1001', 'MF', 'Alberto', 'Medina', TO_DATE('1983-05-29', 'YYYY-MM-DD'), 56, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1045', 'T1001', 'GK', 'Luis-Ernesto', 'Michel', TO_DATE('1979-07-21', 'YYYY-MM-DD'), 4, 'Mexico', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1046', 'T1000', 'GK', 'Fernando', 'Muslera', TO_DATE('1986-06-16', 'YYYY-MM-DD'), 6, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1047', 'T1000', 'DF', 'Diego', 'Lugano', TO_DATE('1980-11-02', 'YYYY-MM-DD'), 42, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1048', 'T1000', 'DF', 'Diego', 'God�n', TO_DATE('1986-02-16', 'YYYY-MM-DD'), 38, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1049', 'T1000', 'DF', 'Jorge', 'Fucile', TO_DATE('1984-11-19', 'YYYY-MM-DD'), 24, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1050', 'T1000', 'MF', 'Walter', 'Gargano', TO_DATE('1984-07-23', 'YYYY-MM-DD'), 28, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1051', 'T1000', 'DF', 'Mauricio', 'Victorino', TO_DATE('1982-10-11', 'YYYY-MM-DD'), 4, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1052', 'T1000', 'FW', 'Edinson', 'Cavani', TO_DATE('1987-02-14', 'YYYY-MM-DD'), 14, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1053', 'T1000', 'MF', 'Sebasti�n', 'Eguren', TO_DATE('1981-01-08', 'YYYY-MM-DD'), 27, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1054', 'T1000', 'FW', 'Luis', 'Su�rez', TO_DATE('1987-01-24', 'YYYY-MM-DD'), 30, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1055', 'T1000', 'FW', 'Diego', 'Forl�n', TO_DATE('1979-05-19', 'YYYY-MM-DD'), 62, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1056', 'T1000', 'MF', '�lvaro', 'Pereira', TO_DATE('1985-11-28', 'YYYY-MM-DD'), 15, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1057', 'T1000', 'GK', 'Juan', 'Castillo', TO_DATE('1978-04-17', 'YYYY-MM-DD'), 11, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1058', 'T1000', 'FW', 'Sebasti�n', 'Abreu', TO_DATE('1976-10-17', 'YYYY-MM-DD'), 56, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1059', 'T1000', 'MF', 'Nicol�s', 'Lodeiro', TO_DATE('1989-03-21', 'YYYY-MM-DD'), 4, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1060', 'T1000', 'MF', 'Diego', 'P�rez', TO_DATE('1980-05-18', 'YYYY-MM-DD'), 50, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1061', 'T1000', 'DF', 'Maxi', 'Pereira', TO_DATE('1984-06-08', 'YYYY-MM-DD'), 37, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1062', 'T1000', 'MF', 'Egidio', 'Ar�valo-R�os', TO_DATE('1982-01-01', 'YYYY-MM-DD'), 6, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1063', 'T1000', 'DF', 'Andr�s', 'Scotti', TO_DATE('1974-12-14', 'YYYY-MM-DD'), 26, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1064', 'T1000', 'MF', '�lvaro', 'Fern�ndez', TO_DATE('1985-10-11', 'YYYY-MM-DD'), 7, 'Uruguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1065', 'T1000', 'FW', 'Sebasti�n', 'Fern�ndez', TO_DATE('1985-05-23', 'YYYY-MM-DD'), 6, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1066', 'T1000', 'DF', 'Mart�n', 'C�ceres', TO_DATE('1987-04-07', 'YYYY-MM-DD'), 19, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1067', 'T1000', 'GK', 'Mart�n', 'Silva', TO_DATE('1983-03-25', 'YYYY-MM-DD'), 1, 'Uruguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1068', 'T1003', 'GK', 'Hugo', 'Lloris', TO_DATE('1986-12-26', 'YYYY-MM-DD'), 11, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1069', 'T1003', 'DF', 'Bacary', 'Sagna', TO_DATE('1983-02-14', 'YYYY-MM-DD'), 20, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1070', 'T1003', 'DF', '�ric', 'Abidal', TO_DATE('1979-09-11', 'YYYY-MM-DD'), 54, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1071', 'T1003', 'DF', 'Anthony', 'R�veill�re', TO_DATE('1979-11-10', 'YYYY-MM-DD'), 13, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1072', 'T1003', 'DF', 'William', 'Gallas', TO_DATE('1977-08-17', 'YYYY-MM-DD'), 81, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1073', 'T1003', 'DF', 'Marc', 'Planus', TO_DATE('1982-03-07', 'YYYY-MM-DD'), 1, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1074', 'T1003', 'MF', 'Franck', 'Rib�ry', TO_DATE('1983-04-07', 'YYYY-MM-DD'), 45, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1075', 'T1003', 'MF', 'Yoann', 'Gourcuff', TO_DATE('1986-07-11', 'YYYY-MM-DD'), 20, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1076', 'T1003', 'FW', 'Djibril', 'Ciss�', TO_DATE('1981-08-12', 'YYYY-MM-DD'), 39, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1077', 'T1003', 'FW', 'Sidney', 'Govou', TO_DATE('1979-07-27', 'YYYY-MM-DD'), 46, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1078', 'T1003', 'FW', 'Andr�-Pierre', 'Gignac', TO_DATE('1985-12-05', 'YYYY-MM-DD'), 13, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1079', 'T1003', 'FW', 'Thierry', 'Henry', TO_DATE('1977-08-17', 'YYYY-MM-DD'), 21, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1080', 'T1003', 'DF', 'Patrice', 'Evra', TO_DATE('1981-05-15', 'YYYY-MM-DD'), 30, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1081', 'T1003', 'MF', 'J�r�my', 'Toulalan', TO_DATE('1983-09-10', 'YYYY-MM-DD'), 34, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1082', 'T1003', 'MF', 'Florent', 'Malouda', TO_DATE('1980-06-13', 'YYYY-MM-DD'), 55, 'France', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1083', 'T1003', 'GK', 'Steve', 'Mandanda', TO_DATE('1985-03-28', 'YYYY-MM-DD'), 13, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1084', 'T1003', 'DF', 'S�bastien', 'Squillaci', TO_DATE('1980-08-11', 'YYYY-MM-DD'), 20, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1085', 'T1003', 'MF', 'Alou', 'Diarra', TO_DATE('1981-07-15', 'YYYY-MM-DD'), 25, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1086', 'T1003', 'MF', 'Abou', 'Diaby', TO_DATE('1986-05-11', 'YYYY-MM-DD'), 5, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1087', 'T1003', 'MF', 'Mathieu', 'Valbuena', TO_DATE('1984-09-28', 'YYYY-MM-DD'), 2, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1088', 'T1003', 'FW', 'Nicolas', 'Anelka', TO_DATE('1979-03-14', 'YYYY-MM-DD'), 67, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1089', 'T1003', 'DF', 'Ga�l', 'Clichy', TO_DATE('1985-07-26', 'YYYY-MM-DD'), 4, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1090', 'T1003', 'GK', 'C�dric', 'Carrasso', TO_DATE('1981-12-30', 'YYYY-MM-DD'), 42, 'France', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1091', 'T1004', 'GK', 'Diego', 'Pozo', TO_DATE('1978-02-16', 'YYYY-MM-DD'), 3, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1092', 'T1004', 'DF', 'Mart�n', 'Demichelis', TO_DATE('1980-12-20', 'YYYY-MM-DD'), 25, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1093', 'T1004', 'DF', 'Clemente', 'Rodr�guez', TO_DATE('1981-07-31', 'YYYY-MM-DD'), 11, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1094', 'T1004', 'DF', 'Nicol�s', 'Burdisso', TO_DATE('1981-04-12', 'YYYY-MM-DD'), 28, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1095', 'T1004', 'MF', 'Mario', 'Bolatti', TO_DATE('1985-02-17', 'YYYY-MM-DD'), 4, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1096', 'T1004', 'DF', 'Gabriel', 'Heinze', TO_DATE('1978-04-19', 'YYYY-MM-DD'), 63, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1097', 'T1004', 'MF', '�ngel', 'di-Mar�a', TO_DATE('1988-02-14', 'YYYY-MM-DD'), 7, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1098', 'T1004', 'MF', 'Juan-Sebasti�n', 'Ver�n', TO_DATE('1975-03-09', 'YYYY-MM-DD'), 69, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1099', 'T1004', 'FW', 'Gonzalo', 'Higua�n', TO_DATE('1987-12-10', 'YYYY-MM-DD'), 4, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1100', 'T1004', 'FW', 'Lionel', 'Messi', TO_DATE('1987-06-24', 'YYYY-MM-DD'), 44, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1101', 'T1004', 'FW', 'Carlos', 'Tevez', TO_DATE('1984-02-05', 'YYYY-MM-DD'), 51, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1102', 'T1004', 'DF', 'Ariel', 'Garc�', TO_DATE('1979-07-14', 'YYYY-MM-DD'), 3, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1103', 'T1004', 'DF', 'Walter', 'Samuel', TO_DATE('1978-03-23', 'YYYY-MM-DD'), 54, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1104', 'T1004', 'MF', 'Javier', 'Mascherano', TO_DATE('1984-06-08', 'YYYY-MM-DD'), 56, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1105', 'T1004', 'DF', 'Nicol�s', 'Otamendi', TO_DATE('1988-02-12', 'YYYY-MM-DD'), 6, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1106', 'T1004', 'FW', 'Sergio', 'Ag�ero', TO_DATE('1988-06-02', 'YYYY-MM-DD'), 20, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1107', 'T1004', 'MF', 'Jon�s', 'Guti�rrez', TO_DATE('1983-07-05', 'YYYY-MM-DD'), 15, 'Argentina', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1108', 'T1004', 'FW', 'Mart�n', 'Palermo', TO_DATE('1973-11-07', 'YYYY-MM-DD'), 14, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1109', 'T1004', 'FW', 'Diego', 'Milito', TO_DATE('1979-06-12', 'YYYY-MM-DD'), 20, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1110', 'T1004', 'MF', 'Maxi', 'Rodr�guez', TO_DATE('1981-01-02', 'YYYY-MM-DD'), 35, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1111', 'T1004', 'GK', 'Mariano', 'And�jar', TO_DATE('1983-07-30', 'YYYY-MM-DD'), 4, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1112', 'T1004', 'GK', 'Sergio', 'Romero', TO_DATE('1987-02-22', 'YYYY-MM-DD'), 5, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1113', 'T1004', 'MF', 'Javier', 'Pastore', TO_DATE('1989-06-20', 'YYYY-MM-DD'), 4, 'Argentina', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1114', 'T1007', 'GK', 'Vincent', 'Enyeama', TO_DATE('1982-08-29', 'YYYY-MM-DD'), 51, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1115', 'T1007', 'DF', 'Joseph', 'Yobo', TO_DATE('1980-09-06', 'YYYY-MM-DD'), 64, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1116', 'T1007', 'DF', 'Taye', 'Taiwo', TO_DATE('1985-04-16', 'YYYY-MM-DD'), 35, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1117', 'T1007', 'FW', 'Nwankwo', 'Kanu', TO_DATE('1976-08-01', 'YYYY-MM-DD'), 72, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1118', 'T1007', 'DF', 'Rabiu', 'Afolabi', TO_DATE('1980-04-18', 'YYYY-MM-DD'), 12, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1119', 'T1007', 'DF', 'Danny', 'Shittu', TO_DATE('1980-09-02', 'YYYY-MM-DD'), 23, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1120', 'T1007', 'FW', 'John', 'Utaka', TO_DATE('1982-01-08', 'YYYY-MM-DD'), 41, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1121', 'T1007', 'FW', 'Yakubu', 'Aiyegbeni', TO_DATE('1982-11-22', 'YYYY-MM-DD'), 47, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1122', 'T1007', 'FW', 'Obafemi', 'Martins', TO_DATE('1984-10-28', 'YYYY-MM-DD'), 27, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1123', 'T1007', 'FW', 'Brown', 'Ideye', TO_DATE('1988-10-10', 'YYYY-MM-DD'), 10, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1124', 'T1007', 'FW', 'Peter', 'Odemwingie', TO_DATE('1981-07-15', 'YYYY-MM-DD'), 43, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1125', 'T1007', 'FW', 'Kalu', 'Uche', TO_DATE('1982-11-15', 'YYYY-MM-DD'), 18, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1126', 'T1007', 'MF', 'Ayila', 'Yussuf', TO_DATE('1984-11-04', 'YYYY-MM-DD'), 22, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1127', 'T1007', 'MF', 'Sani', 'Kaita', TO_DATE('1986-05-02', 'YYYY-MM-DD'), 16, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1128', 'T1007', 'MF', 'Lukman', 'Haruna', TO_DATE('1990-04-12', 'YYYY-MM-DD'), 5, 'Nigeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1129', 'T1007', 'GK', 'Austin', 'Ejide', TO_DATE('1984-04-08', 'YYYY-MM-DD'), 16, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1130', 'T1007', 'DF', 'Chidi', 'Odiah', TO_DATE('1983-12-17', 'YYYY-MM-DD'), 21, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1131', 'T1007', 'FW', 'Victor', 'Obinna', TO_DATE('1987-03-25', 'YYYY-MM-DD'), 30, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1132', 'T1007', 'FW', 'Chinedu', 'Obasi', TO_DATE('1986-06-01', 'YYYY-MM-DD'), 18, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1133', 'T1007', 'MF', 'Dickson', 'Etuhu', TO_DATE('1982-06-08', 'YYYY-MM-DD'), 11, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1134', 'T1007', 'DF', 'Uwa', 'Echi�jil�', TO_DATE('1988-01-20', 'YYYY-MM-DD'), 9, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1135', 'T1007', 'DF', 'Dele', 'Adeleye', TO_DATE('1988-12-25', 'YYYY-MM-DD'), 5, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1136', 'T1007', 'GK', 'Dele', 'Aiyenugba', TO_DATE('1983-11-20', 'YYYY-MM-DD'), 9, 'Nigeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1137', 'T1005', 'GK', 'Lee', 'Woon-Jae', TO_DATE('1973-04-26', 'YYYY-MM-DD'), 30, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1138', 'T1005', 'DF', 'Oh', 'Beom-Seok', TO_DATE('1984-07-29', 'YYYY-MM-DD'), 37, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1139', 'T1005', 'DF', 'Kim', 'Hyung-Il', TO_DATE('1984-04-27', 'YYYY-MM-DD'), 2, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1140', 'T1005', 'MF', 'Cho', 'Yong-Hyung', TO_DATE('1983-11-03', 'YYYY-MM-DD'), 31, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1141', 'T1005', 'MF', 'Kim', 'Nam-Il', TO_DATE('1977-03-14', 'YYYY-MM-DD'), 92, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1142', 'T1005', 'MF', 'Kim', 'Bo-Kyung', TO_DATE('1989-10-06', 'YYYY-MM-DD'), 6, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1143', 'T1005', 'MF', 'Park', 'Ji-Sung', TO_DATE('1981-02-25', 'YYYY-MM-DD'), 88, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1144', 'T1005', 'FW', 'Kim', 'Jung-Woo', TO_DATE('1982-05-09', 'YYYY-MM-DD'), 54, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1145', 'T1005', 'FW', 'Ahn', 'Jung-Hwan', TO_DATE('1976-01-27', 'YYYY-MM-DD'), 70, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1146', 'T1005', 'FW', 'Park', 'Chu-Young', TO_DATE('1985-07-10', 'YYYY-MM-DD'), 40, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1147', 'T1005', 'FW', 'Lee', 'Seung-Yeoul', TO_DATE('1989-03-06', 'YYYY-MM-DD'), 8, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1148', 'T1005', 'DF', 'Lee', 'Young-Pyo', TO_DATE('1977-04-23', 'YYYY-MM-DD'), 12, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1149', 'T1005', 'MF', 'Kim', 'Jae-Sung', TO_DATE('1983-10-03', 'YYYY-MM-DD'), 7, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1150', 'T1005', 'DF', 'Lee', 'Jung-Soo', TO_DATE('1980-01-08', 'YYYY-MM-DD'), 24, 'Korea Republic', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1151', 'T1005', 'DF', 'Kim', 'Dong-Jin', TO_DATE('1982-01-29', 'YYYY-MM-DD'), 61, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1152', 'T1005', 'MF', 'Ki', 'Sung-Yong', TO_DATE('1989-01-24', 'YYYY-MM-DD'), 21, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1153', 'T1005', 'MF', 'Lee', 'Chung-Yong', TO_DATE('1988-07-02', 'YYYY-MM-DD'), 23, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1154', 'T1005', 'GK', 'Jung', 'Sung-Ryong', TO_DATE('1985-01-04', 'YYYY-MM-DD'), 15, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1155', 'T1005', 'FW', 'Yeom', 'Ki-Hun', TO_DATE('1983-03-30', 'YYYY-MM-DD'), 33, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1156', 'T1005', 'FW', 'Lee', 'Dong-Gook', TO_DATE('1979-04-29', 'YYYY-MM-DD'), 83, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1157', 'T1005', 'GK', 'Kim', 'Young-Kwang', TO_DATE('1983-06-28', 'YYYY-MM-DD'), 14, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1158', 'T1005', 'DF', 'Cha', 'Du-Ri', TO_DATE('1980-07-25', 'YYYY-MM-DD'), 46, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1159', 'T1005', 'DF', 'Kang', 'Min-Soo', TO_DATE('1986-02-14', 'YYYY-MM-DD'), 31, 'Korea Republic', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1160', 'T1006', 'GK', 'Kostas', 'Chalkias', TO_DATE('1974-05-30', 'YYYY-MM-DD'), 27, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1161', 'T1006', 'DF', 'Giourkas', 'Seitaridis', TO_DATE('1981-06-04', 'YYYY-MM-DD'), 67, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1162', 'T1006', 'DF', 'Christos', 'Patsatzoglou', TO_DATE('1979-03-19', 'YYYY-MM-DD'), 41, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1163', 'T1006', 'MF', 'Nikos', 'Spiropoulos', TO_DATE('1983-10-10', 'YYYY-MM-DD'), 17, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1164', 'T1006', 'DF', 'Vangelis', 'Moras', TO_DATE('1981-08-18', 'YYYY-MM-DD'), 10, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1165', 'T1006', 'MF', 'Alexandros', 'Tziolis', TO_DATE('1985-02-13', 'YYYY-MM-DD'), 18, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1166', 'T1006', 'FW', 'Georgios', 'Samaras', TO_DATE('1985-02-21', 'YYYY-MM-DD'), 32, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1167', 'T1006', 'DF', 'Avraam', 'Papadopoulos', TO_DATE('1984-01-03', 'YYYY-MM-DD'), 12, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1168', 'T1006', 'FW', 'Angelos', 'Charisteas', TO_DATE('1980-02-09', 'YYYY-MM-DD'), 82, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1169', 'T1006', 'MF', 'Giorgos', 'Karagounis', TO_DATE('1977-03-06', 'YYYY-MM-DD'), 91, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1170', 'T1006', 'DF', 'Loukas', 'Vyntra', TO_DATE('1981-02-05', 'YYYY-MM-DD'), 27, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1171', 'T1006', 'GK', 'Alexandros', 'Tzorvas', TO_DATE('1982-08-12', 'YYYY-MM-DD'), 6, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1172', 'T1006', 'GK', 'Michalis', 'Sifakis', TO_DATE('1984-09-09', 'YYYY-MM-DD'), 1, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1173', 'T1006', 'FW', 'Dimitris', 'Salpingidis', TO_DATE('1981-08-18', 'YYYY-MM-DD'), 34, 'Greece', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1174', 'T1006', 'DF', 'Vasilis', 'Torosidis', TO_DATE('1985-06-10', 'YYYY-MM-DD'), 25, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1175', 'T1006', 'DF', 'Sotirios', 'Kyrgiakos', TO_DATE('1979-07-23', 'YYYY-MM-DD'), 56, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1176', 'T1006', 'FW', 'Theofanis', 'Gekas', TO_DATE('1980-05-23', 'YYYY-MM-DD'), 46, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1177', 'T1006', 'MF', 'Sotiris', 'Ninis', TO_DATE('1990-04-03', 'YYYY-MM-DD'), 3, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1178', 'T1006', 'DF', 'Sokratis', 'Papastathopoulos', TO_DATE('1988-06-09', 'YYYY-MM-DD'), 10, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1179', 'T1006', 'FW', 'Pantelis', 'Kapetanos', TO_DATE('1983-06-08', 'YYYY-MM-DD'), 3, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1180', 'T1006', 'MF', 'Kostas', 'Katsouranis', TO_DATE('1979-06-21', 'YYYY-MM-DD'), 67, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1181', 'T1006', 'DF', 'Stelios', 'Malezas', TO_DATE('1985-03-11', 'YYYY-MM-DD'), 10, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1182', 'T1006', 'MF', 'Sakis', 'Prittas', TO_DATE('1979-01-09', 'YYYY-MM-DD'), 20, 'Greece', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1183', 'T1009', 'GK', 'David', 'James', TO_DATE('1970-08-01', 'YYYY-MM-DD'), 50, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1184', 'T1009', 'DF', 'Glen', 'Johnson', TO_DATE('1984-08-23', 'YYYY-MM-DD'), 22, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1185', 'T1009', 'DF', 'Ashley', 'Cole', TO_DATE('1980-12-20', 'YYYY-MM-DD'), 78, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1186', 'T1009', 'MF', 'Steven', 'Gerrard', TO_DATE('1980-05-30', 'YYYY-MM-DD'), 80, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1187', 'T1009', 'DF', 'Michael', 'Dawson', TO_DATE('1983-11-18', 'YYYY-MM-DD'), 30, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1188', 'T1009', 'DF', 'John', 'Terry', TO_DATE('1980-12-07', 'YYYY-MM-DD'), 60, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1189', 'T1009', 'MF', 'Aaron', 'Lennon', TO_DATE('1987-04-16', 'YYYY-MM-DD'), 17, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1190', 'T1009', 'MF', 'Frank', 'Lampard', TO_DATE('1978-06-20', 'YYYY-MM-DD'), 78, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1191', 'T1009', 'FW', 'Peter', 'Crouch', TO_DATE('1981-01-30', 'YYYY-MM-DD'), 38, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1192', 'T1009', 'FW', 'Wayne', 'Rooney', TO_DATE('1985-10-24', 'YYYY-MM-DD'), 60, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1193', 'T1009', 'MF', 'Joe', 'Cole', TO_DATE('1981-11-08', 'YYYY-MM-DD'), 54, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1194', 'T1009', 'GK', 'Robert', 'Green', TO_DATE('1980-01-18', 'YYYY-MM-DD'), 10, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1195', 'T1009', 'DF', 'Stephen', 'Warnock', TO_DATE('1981-12-12', 'YYYY-MM-DD'), 1, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1196', 'T1009', 'MF', 'Gareth', 'Barry', TO_DATE('1981-02-23', 'YYYY-MM-DD'), 36, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1197', 'T1009', 'DF', 'Matthew', 'Upson', TO_DATE('1979-04-18', 'YYYY-MM-DD'), 19, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1198', 'T1009', 'MF', 'James', 'Milner', TO_DATE('1986-01-04', 'YYYY-MM-DD'), 8, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1199', 'T1009', 'MF', 'Shaun', 'Wright', TO_DATE('1981-10-25', 'YYYY-MM-DD'), 31, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1200', 'T1009', 'DF', 'Jamie', 'Carragher', TO_DATE('1978-01-28', 'YYYY-MM-DD'), 36, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1201', 'T1009', 'FW', 'Jermain', 'Defoe', TO_DATE('1982-10-07', 'YYYY-MM-DD'), 39, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1202', 'T1009', 'DF', 'Ledley', 'King', TO_DATE('1980-10-12', 'YYYY-MM-DD'), 20, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1203', 'T1009', 'FW', 'Emile', 'Heskey', TO_DATE('1978-01-11', 'YYYY-MM-DD'), 58, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1204', 'T1009', 'MF', 'Michael', 'Carrick', TO_DATE('1981-07-28', 'YYYY-MM-DD'), 22, 'England', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1205', 'T1009', 'GK', 'Joe', 'Hart', TO_DATE('1987-04-19', 'YYYY-MM-DD'), 3, 'England', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1206', 'T1008', 'GK', 'Tim', 'Howard', TO_DATE('1979-03-06', 'YYYY-MM-DD'), 51, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1207', 'T1008', 'DF', 'Jonathan', 'Spector', TO_DATE('1986-03-01', 'YYYY-MM-DD'), 25, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1208', 'T1008', 'DF', 'Carlos', 'Bocanegra', TO_DATE('1979-05-25', 'YYYY-MM-DD'), 79, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1209', 'T1008', 'MF', 'Michael', 'Bradley', TO_DATE('1987-07-31', 'YYYY-MM-DD'), 43, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1210', 'T1008', 'DF', 'Oguchi', 'Onyewu', TO_DATE('1982-05-13', 'YYYY-MM-DD'), 54, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1211', 'T1008', 'DF', 'Steve', 'Cherundolo', TO_DATE('1979-02-19', 'YYYY-MM-DD'), 60, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1212', 'T1008', 'MF', 'DaMarcus', 'Beasley', TO_DATE('1982-05-24', 'YYYY-MM-DD'), 92, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1213', 'T1008', 'MF', 'Clint', 'Dempsey', TO_DATE('1983-03-09', 'YYYY-MM-DD'), 62, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1214', 'T1008', 'FW', 'Herculez', 'Gomez', TO_DATE('1982-04-06', 'YYYY-MM-DD'), 4, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1215', 'T1008', 'MF', 'Landon', 'Donovan', TO_DATE('1982-03-04', 'YYYY-MM-DD'), 23, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1216', 'T1008', 'MF', 'Stuart', 'Holden', TO_DATE('1985-08-01', 'YYYY-MM-DD'), 14, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1217', 'T1008', 'DF', 'Jonathan', 'Bornstein', TO_DATE('1984-11-07', 'YYYY-MM-DD'), 32, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1218', 'T1008', 'MF', 'Ricardo', 'Clark', TO_DATE('1983-03-10', 'YYYY-MM-DD'), 29, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1219', 'T1008', 'FW', 'Edson', 'Buddle', TO_DATE('1981-05-21', 'YYYY-MM-DD'), 3, 'USA', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1220', 'T1008', 'DF', 'Jay', 'DeMeri', TO_DATE('1979-12-04', 'YYYY-MM-DD'), 19, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1221', 'T1008', 'MF', 'Jos�', 'Torres', TO_DATE('1987-10-29', 'YYYY-MM-DD'), 10, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1222', 'T1008', 'FW', 'Jozy', 'Altidore', TO_DATE('1989-11-06', 'YYYY-MM-DD'), 25, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1223', 'T1008', 'GK', 'Brad', 'Guzan', TO_DATE('1984-09-09', 'YYYY-MM-DD'), 16, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1224', 'T1008', 'MF', 'Maurice', 'Edu', TO_DATE('1986-04-18', 'YYYY-MM-DD'), 13, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1225', 'T1008', 'FW', 'Robbie', 'Findley', TO_DATE('1985-08-04', 'YYYY-MM-DD'), 6, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1226', 'T1008', 'DF', 'Clarence', 'Goodson', TO_DATE('1982-05-17', 'YYYY-MM-DD'), 14, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1227', 'T1008', 'MF', 'Benny', 'Feilhaber', TO_DATE('1985-01-19', 'YYYY-MM-DD'), 32, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1228', 'T1008', 'GK', 'Marcus', 'Hahnemann', TO_DATE('1972-06-15', 'YYYY-MM-DD'), 7, 'USA', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1229', 'T1011', 'GK', 'Loun�s', 'Gaouaoui', TO_DATE('1977-09-28', 'YYYY-MM-DD'), 48, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1230', 'T1011', 'DF', 'Madjid', 'Bougherra', TO_DATE('1982-10-07', 'YYYY-MM-DD'), 40, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1231', 'T1011', 'DF', 'Nadir', 'Belhadj', TO_DATE('1982-06-18', 'YYYY-MM-DD'), 44, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1232', 'T1011', 'DF', 'Antar', 'Yahia', TO_DATE('1982-03-21', 'YYYY-MM-DD'), 43, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1233', 'T1011', 'DF', 'Rafik', 'Halliche', TO_DATE('1986-09-02', 'YYYY-MM-DD'), 15, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1234', 'T1011', 'MF', 'Yazid', 'Mansouri', TO_DATE('1978-02-25', 'YYYY-MM-DD'), 66, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1235', 'T1011', 'MF', 'Ryad', 'Boudebouz', TO_DATE('1990-02-19', 'YYYY-MM-DD'), 1, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1236', 'T1011', 'MF', 'Medhi', 'Lacen', TO_DATE('1984-03-15', 'YYYY-MM-DD'), 2, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1237', 'T1011', 'FW', 'Abdelkader', 'Ghezzal', TO_DATE('1984-12-05', 'YYYY-MM-DD'), 18, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1238', 'T1011', 'FW', 'Rafik', 'Sa�fi', TO_DATE('1975-02-07', 'YYYY-MM-DD'), 59, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1239', 'T1011', 'FW', 'Rafik', 'Djebbour', TO_DATE('1984-03-08', 'YYYY-MM-DD'), 15, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1240', 'T1011', 'DF', 'Habib', 'Bella�d', TO_DATE('1986-03-28', 'YYYY-MM-DD'), 1, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1241', 'T1011', 'FW', 'Karim', 'Matmour', TO_DATE('1984-06-25', 'YYYY-MM-DD'), 21, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1242', 'T1011', 'DF', 'Abdelkader', 'La�faoui', TO_DATE('1981-07-29', 'YYYY-MM-DD'), 6, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1243', 'T1011', 'MF', 'Karim', 'Ziani', TO_DATE('1982-08-17', 'YYYY-MM-DD'), 54, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1244', 'T1011', 'GK', 'Faouzi', 'Chaouchi', TO_DATE('1984-12-05', 'YYYY-MM-DD'), 9, 'Algeria', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1245', 'T1011', 'MF', 'Adl�ne', 'Guedioura', TO_DATE('1985-11-12', 'YYYY-MM-DD'), 1, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1246', 'T1011', 'DF', 'Carl', 'Medjani', TO_DATE('1985-05-15', 'YYYY-MM-DD'), 10, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1247', 'T1011', 'MF', 'Hassan', 'Yebda', TO_DATE('1984-04-14', 'YYYY-MM-DD'), 9, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1248', 'T1011', 'DF', 'Djamel', 'Mesbah', TO_DATE('1984-10-09', 'YYYY-MM-DD'), 1, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1249', 'T1011', 'MF', 'Foued', 'Kadir', TO_DATE('1983-12-05', 'YYYY-MM-DD'), 1, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1250', 'T1011', 'MF', 'Djamel', 'Abdoun', TO_DATE('1986-02-14', 'YYYY-MM-DD'), 6, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1251', 'T1011', 'GK', 'Ra�s', 'M`Bohli', TO_DATE('1986-04-25', 'YYYY-MM-DD'), 1, 'Algeria', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1252', 'T1010', 'GK', 'Samir', 'Handanovic', TO_DATE('1984-07-14', 'YYYY-MM-DD'), 38, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1253', 'T1010', 'DF', 'Mi�o', 'Brecko', TO_DATE('1984-05-01', 'YYYY-MM-DD'), 30, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1254', 'T1010', 'DF', 'Elvedin', 'D�inic', TO_DATE('1985-08-25', 'YYYY-MM-DD'), 20, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1255', 'T1010', 'DF', 'Marko', '�uler', TO_DATE('1983-03-09', 'YYYY-MM-DD'), 16, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1256', 'T1010', 'DF', 'Bo�tjan', 'Cesar', TO_DATE('1982-07-09', 'YYYY-MM-DD'), 41, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1257', 'T1010', 'DF', 'Branko', 'Ilic', TO_DATE('1983-02-06', 'YYYY-MM-DD'), 36, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1258', 'T1010', 'FW', 'Nejc', 'Pecnik', TO_DATE('1986-01-03', 'YYYY-MM-DD'), 7, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1259', 'T1010', 'MF', 'Robert', 'Koren', TO_DATE('1980-09-20', 'YYYY-MM-DD'), 45, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1260', 'T1010', 'FW', 'Zlatan', 'Ljubijankic', TO_DATE('1983-12-15', 'YYYY-MM-DD'), 16, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1261', 'T1010', 'FW', 'Valter', 'Birsa', TO_DATE('1986-08-07', 'YYYY-MM-DD'), 33, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1262', 'T1010', 'FW', 'Milivoje', 'Novakovic', TO_DATE('1979-05-18', 'YYYY-MM-DD'), 37, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1263', 'T1010', 'GK', 'Jasmin', 'Handanovic', TO_DATE('1978-01-28', 'YYYY-MM-DD'), 3, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1264', 'T1010', 'DF', 'Bojan', 'Jokic', TO_DATE('1986-05-17', 'YYYY-MM-DD'), 34, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1265', 'T1010', 'FW', 'Zlatko', 'Dedic', TO_DATE('1984-05-10', 'YYYY-MM-DD'), 23, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1266', 'T1010', 'MF', 'Rene', 'Krhin', TO_DATE('1990-05-21', 'YYYY-MM-DD'), 3, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1267', 'T1010', 'GK', 'Aleksander', '�eliga', TO_DATE('1980-02-01', 'YYYY-MM-DD'), 1, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1268', 'T1010', 'MF', 'Andra�', 'Kirm', TO_DATE('1984-09-06', 'YYYY-MM-DD'), 25, 'Slovenia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1269', 'T1010', 'MF', 'Aleksandar', 'Radosavljevic', TO_DATE('1979-04-25', 'YYYY-MM-DD'), 14, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1270', 'T1010', 'DF', 'Suad', 'Filekovic', TO_DATE('1978-09-16', 'YYYY-MM-DD'), 15, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1271', 'T1010', 'MF', 'Andrej', 'Komac', TO_DATE('1979-12-04', 'YYYY-MM-DD'), 40, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1272', 'T1010', 'MF', 'Dalibor', 'Stevanovic', TO_DATE('1984-09-27', 'YYYY-MM-DD'), 15, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1273', 'T1010', 'DF', 'Matej', 'Mavric', TO_DATE('1979-01-29', 'YYYY-MM-DD'), 32, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1274', 'T1010', 'FW', 'Tim', 'Matav�', TO_DATE('1989-01-13', 'YYYY-MM-DD'), 11, 'Slovenia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1275', 'T1012', 'GK', 'Manuel', 'Neuer', TO_DATE('1986-03-27', 'YYYY-MM-DD'), 5, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1276', 'T1012', 'DF', 'Marcell', 'Jansen', TO_DATE('1985-11-04', 'YYYY-MM-DD'), 31, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1277', 'T1012', 'DF', 'Arne', 'Friedrich', TO_DATE('1979-05-29', 'YYYY-MM-DD'), 72, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1278', 'T1012', 'DF', 'Dennis', 'Aogo', TO_DATE('1987-01-14', 'YYYY-MM-DD'), 2, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1279', 'T1012', 'DF', 'Serdar', 'Tasci', TO_DATE('1987-04-24', 'YYYY-MM-DD'), 12, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1280', 'T1012', 'MF', 'Sami', 'Khedira', TO_DATE('1987-04-04', 'YYYY-MM-DD'), 5, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1281', 'T1012', 'MF', 'Bastian', 'Schweinsteiger', TO_DATE('1984-08-01', 'YYYY-MM-DD'), 74, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1282', 'T1012', 'MF', 'Mesut', '�zil', TO_DATE('1988-10-15', 'YYYY-MM-DD'), 10, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1283', 'T1012', 'FW', 'Stefan', 'Kie�ling', TO_DATE('1984-01-25', 'YYYY-MM-DD'), 4, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1284', 'T1012', 'MF', 'Lukas', 'Podolski', TO_DATE('1985-06-04', 'YYYY-MM-DD'), 73, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1285', 'T1012', 'FW', 'Miroslav', 'Klose', TO_DATE('1978-06-09', 'YYYY-MM-DD'), 96, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1286', 'T1012', 'GK', 'Tim', 'Wiese', TO_DATE('1981-12-17', 'YYYY-MM-DD'), 2, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1287', 'T1012', 'MF', 'Thomas', 'M�ller', TO_DATE('1989-09-13', 'YYYY-MM-DD'), 2, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1288', 'T1012', 'DF', 'Holger', 'Badstuber', TO_DATE('1989-03-13', 'YYYY-MM-DD'), 2, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1289', 'T1012', 'MF', 'Piotr', 'Trochowski', TO_DATE('1984-03-22', 'YYYY-MM-DD'), 31, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1290', 'T1012', 'DF', 'Philipp', 'Lahm', TO_DATE('1983-11-11', 'YYYY-MM-DD'), 65, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1291', 'T1012', 'DF', 'Per', 'Mertesacker', TO_DATE('1984-09-29', 'YYYY-MM-DD'), 62, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1292', 'T1012', 'MF', 'Toni', 'Kroos', TO_DATE('1990-01-04', 'YYYY-MM-DD'), 4, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1293', 'T1012', 'FW', 'Cacau', NULL, TO_DATE('1981-03-27', 'YYYY-MM-DD'), 8, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1294', 'T1012', 'DF', 'J�r�me', 'Boateng', TO_DATE('1988-09-03', 'YYYY-MM-DD'), 5, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1295', 'T1012', 'MF', 'Marko', 'Marin', TO_DATE('1989-03-13', 'YYYY-MM-DD'), 9, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1296', 'T1012', 'GK', 'Hans-J�rg', 'Butt', TO_DATE('1974-05-28', 'YYYY-MM-DD'), 3, 'Germany', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1297', 'T1012', 'FW', 'Mario', 'G�mez', TO_DATE('1985-07-10', 'YYYY-MM-DD'), 34, 'Germany', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1298', 'T1014', 'GK', 'Mark', 'Schwarzer', TO_DATE('1972-10-06', 'YYYY-MM-DD'), 75, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1299', 'T1014', 'DF', 'Lucas', 'Neill', TO_DATE('1978-03-09', 'YYYY-MM-DD'), 56, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1300', 'T1014', 'DF', 'Craig', 'Moore', TO_DATE('1975-12-12', 'YYYY-MM-DD'), 50, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1301', 'T1014', 'MF', 'Tim', 'Cahill', TO_DATE('1979-12-06', 'YYYY-MM-DD'), 40, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1302', 'T1014', 'MF', 'Jason', 'Culina', TO_DATE('1980-08-05', 'YYYY-MM-DD'), 49, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1303', 'T1014', 'DF', 'Michael', 'Beauchamp', TO_DATE('1981-03-08', 'YYYY-MM-DD'), 21, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1304', 'T1014', 'MF', 'Brett', 'Emerton', TO_DATE('1979-02-22', 'YYYY-MM-DD'), 72, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1305', 'T1014', 'DF', 'Luke', 'Wilkshire', TO_DATE('1981-10-01', 'YYYY-MM-DD'), 42, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1306', 'T1014', 'FW', 'Joshua', 'Kennedy', TO_DATE('1982-08-20', 'YYYY-MM-DD'), 19, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1307', 'T1014', 'FW', 'Harry', 'Kewell', TO_DATE('1978-09-22', 'YYYY-MM-DD'), 47, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1308', 'T1014', 'DF', 'Scott', 'Chipperfield', TO_DATE('1975-12-30', 'YYYY-MM-DD'), 65, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1309', 'T1014', 'GK', 'Adam', 'Federici', TO_DATE('1985-01-31', 'YYYY-MM-DD'), 1, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1310', 'T1014', 'MF', 'Vince', 'Grella', TO_DATE('1979-10-05', 'YYYY-MM-DD'), 45, 'Australia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1311', 'T1014', 'FW', 'Brett', 'Holman', TO_DATE('1984-03-27', 'YYYY-MM-DD'), 31, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1312', 'T1014', 'MF', 'Mile', 'Jedinak', TO_DATE('1984-08-03', 'YYYY-MM-DD'), 11, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1313', 'T1014', 'MF', 'Carl', 'Valeri', TO_DATE('1984-08-14', 'YYYY-MM-DD'), 22, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1314', 'T1014', 'FW', 'Nikita', 'Rukavytsya', TO_DATE('1987-06-22', 'YYYY-MM-DD'), 3, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1315', 'T1014', 'GK', 'Eugene', 'Galekovic', TO_DATE('1981-06-12', 'YYYY-MM-DD'), 4, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1316', 'T1014', 'MF', 'Richard', 'Garcia', TO_DATE('1981-09-04', 'YYYY-MM-DD'), 7, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1317', 'T1014', 'DF', 'Mark', 'Milligan', TO_DATE('1985-09-04', 'YYYY-MM-DD'), 10, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1318', 'T1014', 'DF', 'David', 'Carney', TO_DATE('1983-11-03', 'YYYY-MM-DD'), 25, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1319', 'T1014', 'MF', 'Dario', 'Vido�ic', TO_DATE('1987-04-12', 'YYYY-MM-DD'), 7, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1320', 'T1014', 'MF', 'Mark', 'Bresciano', TO_DATE('1980-02-11', 'YYYY-MM-DD'), 55, 'Australia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1321', 'T1015', 'GK', 'Vladimir', 'Stojkovic', TO_DATE('1983-07-29', 'YYYY-MM-DD'), 33, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1322', 'T1015', 'DF', 'Antonio', 'Rukavina', TO_DATE('1984-01-26', 'YYYY-MM-DD'), 20, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1323', 'T1015', 'DF', 'Aleksandar', 'Kolarov', TO_DATE('1985-11-10', 'YYYY-MM-DD'), 12, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1324', 'T1015', 'MF', 'Gojko', 'Kacar', TO_DATE('1987-01-26', 'YYYY-MM-DD'), 17, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1325', 'T1015', 'DF', 'Nemanja', 'Vidic', TO_DATE('1981-10-21', 'YYYY-MM-DD'), 45, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1326', 'T1015', 'DF', 'Branislav', 'Ivanovic', TO_DATE('1984-02-22', 'YYYY-MM-DD'), 31, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1327', 'T1015', 'MF', 'Zoran', 'To�ic', TO_DATE('1987-04-28', 'YYYY-MM-DD'), 21, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1328', 'T1015', 'FW', 'Danko', 'Lazovic', TO_DATE('1983-05-17', 'YYYY-MM-DD'), 37, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1329', 'T1015', 'FW', 'Marko', 'Pantelic', TO_DATE('1978-09-15', 'YYYY-MM-DD'), 32, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1330', 'T1015', 'MF', 'Dejan', 'Stankovic', TO_DATE('1978-09-11', 'YYYY-MM-DD'), 88, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1331', 'T1015', 'MF', 'Nenad', 'Milija�', TO_DATE('1983-04-30', 'YYYY-MM-DD'), 17, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1332', 'T1015', 'GK', 'Bojan', 'Isailovic', TO_DATE('1980-03-25', 'YYYY-MM-DD'), 4, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1333', 'T1015', 'DF', 'Aleksandar', 'Lukovic', TO_DATE('1982-10-23', 'YYYY-MM-DD'), 21, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1334', 'T1015', 'MF', 'Milan', 'Jovanovic', TO_DATE('1981-04-18', 'YYYY-MM-DD'), 26, 'Serbia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1335', 'T1015', 'FW', 'Nikola', '�igic', TO_DATE('1980-09-25', 'YYYY-MM-DD'), 45, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1336', 'T1015', 'DF', 'Ivan', 'Obradovic', TO_DATE('1988-07-25', 'YYYY-MM-DD'), 12, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1337', 'T1015', 'MF', 'Milo�', 'Krasic', TO_DATE('1984-11-01', 'YYYY-MM-DD'), 31, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1338', 'T1015', 'MF', 'Milo�', 'Ninkovic', TO_DATE('1984-12-25', 'YYYY-MM-DD'), 9, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1339', 'T1015', 'MF', 'Radosav', 'Petrovic', TO_DATE('1989-03-08', 'YYYY-MM-DD'), 14, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1340', 'T1015', 'DF', 'Neven', 'Subotic', TO_DATE('1988-12-10', 'YYYY-MM-DD'), 13, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1341', 'T1015', 'FW', 'Dragan', 'Mrda', TO_DATE('1984-01-23', 'YYYY-MM-DD'), 6, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1342', 'T1015', 'MF', 'Zdravko', 'Kuzmanovic', TO_DATE('1987-09-22', 'YYYY-MM-DD'), 27, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1343', 'T1015', 'GK', 'Andelko', '�uricic', TO_DATE('1980-11-21', 'YYYY-MM-DD'), 1, 'Serbia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1344', 'T1013', 'GK', 'Daniel', 'Adjei', TO_DATE('1989-11-10', 'YYYY-MM-DD'), 2, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1345', 'T1013', 'DF', 'Hans', 'Sarpei', TO_DATE('1976-06-28', 'YYYY-MM-DD'), 23, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1346', 'T1013', 'FW', 'Asamoah', 'Gyan', TO_DATE('1985-11-22', 'YYYY-MM-DD'), 32, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1347', 'T1013', 'DF', 'John', 'Paintsil', TO_DATE('1981-06-15', 'YYYY-MM-DD'), 65, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1348', 'T1013', 'DF', 'John', 'Mensah', TO_DATE('1982-11-29', 'YYYY-MM-DD'), 58, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1349', 'T1013', 'MF', 'Anthony', 'Annan', TO_DATE('1986-07-21', 'YYYY-MM-DD'), 38, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1350', 'T1013', 'DF', 'Samuel', 'Inkoom', TO_DATE('1989-08-22', 'YYYY-MM-DD'), 15, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1351', 'T1013', 'DF', 'Jonathan', 'Mensah', TO_DATE('1990-07-13', 'YYYY-MM-DD'), 3, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1352', 'T1013', 'MF', 'Derek', 'Boateng', TO_DATE('1983-04-02', 'YYYY-MM-DD'), 19, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1353', 'T1013', 'MF', 'Stephen', 'Appiah', TO_DATE('1980-12-24', 'YYYY-MM-DD'), 56, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1354', 'T1013', 'MF', 'Sulley', 'Muntari', TO_DATE('1984-08-27', 'YYYY-MM-DD'), 52, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1355', 'T1013', 'FW', 'Prince', 'Tagoe', TO_DATE('1986-11-09', 'YYYY-MM-DD'), 17, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1356', 'T1013', 'MF', 'Andr�', 'Ayew', TO_DATE('1989-12-17', 'YYYY-MM-DD'), 15, 'Ghana', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1357', 'T1013', 'FW', 'Matthew', 'Amoah', TO_DATE('1980-10-24', 'YYYY-MM-DD'), 31, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1358', 'T1013', 'DF', 'Isaac', 'Vorsah', TO_DATE('1988-06-21', 'YYYY-MM-DD'), 6, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1359', 'T1013', 'GK', 'Stephen', 'Ahorlu', TO_DATE('1989-05-10', 'YYYY-MM-DD'), 40, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1360', 'T1013', 'DF', 'Abdul', 'Rahim-Ayew', TO_DATE('1988-04-16', 'YYYY-MM-DD'), 6, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1361', 'T1013', 'FW', 'Dominic', 'Adiyiah', TO_DATE('1989-11-29', 'YYYY-MM-DD'), 4, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1362', 'T1013', 'DF', 'Lee', 'Addy', TO_DATE('1985-09-26', 'YYYY-MM-DD'), 3, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1363', 'T1013', 'MF', 'Quincy', 'Owusu-Abeyie', TO_DATE('1986-04-15', 'YYYY-MM-DD'), 12, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1364', 'T1013', 'MF', 'Kwadwo', 'Asamoah', TO_DATE('1988-09-09', 'YYYY-MM-DD'), 29, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1365', 'T1013', 'GK', 'Richard', 'Kingson', TO_DATE('1978-06-13', 'YYYY-MM-DD'), 58, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1366', 'T1013', 'MF', 'Kevin-Prince', 'Boateng', TO_DATE('1987-03-06', 'YYYY-MM-DD'), 10, 'Ghana', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1367', 'T1016', 'GK', 'Maarten', 'Stekelenburg', TO_DATE('1982-09-22', 'YYYY-MM-DD'), 25, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1368', 'T1016', 'DF', 'Gregory-van-der', 'Wiel', TO_DATE('1988-02-03', 'YYYY-MM-DD'), 8, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1369', 'T1016', 'DF', 'John', 'Heitinga', TO_DATE('1983-11-15', 'YYYY-MM-DD'), 51, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1370', 'T1016', 'DF', 'Joris', 'Mathijsen', TO_DATE('1980-04-05', 'YYYY-MM-DD'), 53, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1371', 'T1016', 'DF', 'Giovanni-van', 'Bronckhorst', TO_DATE('1975-02-05', 'YYYY-MM-DD'), 97, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1372', 'T1016', 'MF', 'Mark-van', 'Bommel', TO_DATE('1977-04-22', 'YYYY-MM-DD'), 54, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1373', 'T1016', 'FW', 'Dirk', 'Kuyt', TO_DATE('1980-07-22', 'YYYY-MM-DD'), 60, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1374', 'T1016', 'MF', 'Nigel', 'de-Jong', TO_DATE('1984-11-30', 'YYYY-MM-DD'), 40, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1375', 'T1016', 'FW', 'Robin', 'Van-Persie', TO_DATE('1983-08-06', 'YYYY-MM-DD'), 7, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1376', 'T1016', 'MF', 'Wesley', 'Sneijder', TO_DATE('1984-06-09', 'YYYY-MM-DD'), 9, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1377', 'T1016', 'FW', 'Arjen', 'Robben', TO_DATE('1984-01-23', 'YYYY-MM-DD'), 46, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1378', 'T1016', 'DF', 'Khalid', 'Boulahrouz', TO_DATE('1981-12-28', 'YYYY-MM-DD'), 28, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1379', 'T1016', 'DF', 'Andr�', 'Ooijer', TO_DATE('1974-07-11', 'YYYY-MM-DD'), 53, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1380', 'T1016', 'MF', 'Demy', 'De-Zeeuw', TO_DATE('1983-05-26', 'YYYY-MM-DD'), 23, 'Netherlands', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1381', 'T1016', 'DF', 'Edson', 'Braafheid', TO_DATE('1983-04-08', 'YYYY-MM-DD'), 5, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1382', 'T1016', 'GK', 'Michel', 'Vorm', TO_DATE('1983-10-20', 'YYYY-MM-DD'), 3, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1383', 'T1016', 'FW', 'Eljero', 'Elia', TO_DATE('1987-02-13', 'YYYY-MM-DD'), 5, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1384', 'T1016', 'MF', 'Stijn', 'Schaars', TO_DATE('1984-01-11', 'YYYY-MM-DD'), 11, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1385', 'T1016', 'FW', 'Ryan', 'Babel', TO_DATE('1986-12-19', 'YYYY-MM-DD'), 38, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1386', 'T1016', 'MF', 'Ibrahim', 'Afellay', TO_DATE('1986-04-02', 'YYYY-MM-DD'), 20, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1387', 'T1016', 'FW', 'Klaas-Jan', 'Huntelaar', TO_DATE('1983-08-12', 'YYYY-MM-DD'), 30, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1388', 'T1016', 'GK', 'Sander', 'Boschker', TO_DATE('1970-10-20', 'YYYY-MM-DD'), 1, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1389', 'T1016', 'MF', 'Rafael', 'Van-der-Vaart', TO_DATE('1983-02-11', 'YYYY-MM-DD'), 75, 'Netherlands', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1390', 'T1018', 'GK', 'Thomas', 'S�rensen', TO_DATE('1976-06-12', 'YYYY-MM-DD'), 86, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1391', 'T1018', 'MF', 'Christian', 'Poulsen', TO_DATE('1980-02-28', 'YYYY-MM-DD'), 72, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1392', 'T1018', 'DF', 'Simon', 'Kj�r', TO_DATE('1989-03-26', 'YYYY-MM-DD'), 9, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1393', 'T1018', 'DF', 'Daniel', 'Agger', TO_DATE('1984-12-12', 'YYYY-MM-DD'), 30, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1394', 'T1018', 'DF', 'William', 'Kvist', TO_DATE('1985-02-24', 'YYYY-MM-DD'), 13, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1395', 'T1018', 'DF', 'Lars', 'Jacobsen', TO_DATE('1979-09-20', 'YYYY-MM-DD'), 29, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1396', 'T1018', 'MF', 'Daniel', 'Jensen', TO_DATE('1979-06-25', 'YYYY-MM-DD'), 47, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1397', 'T1018', 'MF', 'Jesper', 'Gr�nkj�r', TO_DATE('1977-08-12', 'YYYY-MM-DD'), 76, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1398', 'T1018', 'FW', 'Jon-Dahl', 'Tomasson', TO_DATE('1976-08-29', 'YYYY-MM-DD'), 10, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1399', 'T1018', 'MF', 'Martin', 'J�rgensen', TO_DATE('1975-10-06', 'YYYY-MM-DD'), 94, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1400', 'T1018', 'FW', 'Nicklas', 'Bendtner', TO_DATE('1988-01-16', 'YYYY-MM-DD'), 32, 'Denmark', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1401', 'T1018', 'MF', 'Thomas', 'Kahlenberg', TO_DATE('1983-03-20', 'YYYY-MM-DD'), 30, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1402', 'T1018', 'DF', 'Per', 'Kr�ldrup', TO_DATE('1979-07-31', 'YYYY-MM-DD'), 28, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1403', 'T1018', 'MF', 'Jakob', 'Poulsen', TO_DATE('1983-07-07', 'YYYY-MM-DD'), 11, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1404', 'T1018', 'DF', 'Simon', 'Poulsen', TO_DATE('1984-10-07', 'YYYY-MM-DD'), 4, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1405', 'T1018', 'GK', 'Stephan', 'Andersen', TO_DATE('1981-11-26', 'YYYY-MM-DD'), 5, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1406', 'T1018', 'MF', 'Mikkel', 'Beckmann', TO_DATE('1983-10-24', 'YYYY-MM-DD'), 3, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1407', 'T1018', 'FW', 'S�ren', 'Larsen', TO_DATE('1981-09-06', 'YYYY-MM-DD'), 17, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1408', 'T1018', 'MF', 'Dennis', 'Rommedahl', TO_DATE('1978-07-22', 'YYYY-MM-DD'), 94, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1409', 'T1018', 'MF', 'Thomas', 'Enevoldsen', TO_DATE('1987-07-27', 'YYYY-MM-DD'), 4, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1410', 'T1018', 'MF', 'Christian', 'Eriksen', TO_DATE('1992-02-14', 'YYYY-MM-DD'), 2, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1411', 'T1018', 'GK', 'Jesper', 'Christiansen', TO_DATE('1978-04-24', 'YYYY-MM-DD'), 11, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1412', 'T1018', 'DF', 'Patrick', 'Mtiliga', TO_DATE('1981-01-28', 'YYYY-MM-DD'), 2, 'Denmark', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1413', 'T1017', 'GK', 'Seigo', 'Narazaki', TO_DATE('1976-04-15', 'YYYY-MM-DD'), 76, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1414', 'T1017', 'MF', 'Yuki', 'Abe', TO_DATE('1981-09-06', 'YYYY-MM-DD'), 45, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1415', 'T1017', 'DF', 'Yuichi', 'Komano', TO_DATE('1981-07-25', 'YYYY-MM-DD'), 53, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1416', 'T1017', 'DF', 'Marcus', 'Tulio-Tanak', TO_DATE('1981-04-24', 'YYYY-MM-DD'), 39, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1417', 'T1017', 'DF', 'Yuto', 'Nagatomo', TO_DATE('1986-09-12', 'YYYY-MM-DD'), 26, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1418', 'T1017', 'DF', 'Atsuto', 'Uchida', TO_DATE('1988-03-27', 'YYYY-MM-DD'), 31, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1419', 'T1017', 'MF', 'Yasuhito', 'Endo', TO_DATE('1980-01-28', 'YYYY-MM-DD'), 94, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1420', 'T1017', 'FW', 'Daisuke', 'Matsui', TO_DATE('1981-05-11', 'YYYY-MM-DD'), 23, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1421', 'T1017', 'FW', 'Shinji', 'Okazaki', TO_DATE('1986-04-16', 'YYYY-MM-DD'), 28, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1422', 'T1017', 'MF', 'Shunsuke', 'Nakamura', TO_DATE('1978-06-24', 'YYYY-MM-DD'), 97, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1423', 'T1017', 'FW', 'Keiji', 'Tamada', TO_DATE('1980-04-11', 'YYYY-MM-DD'), 70, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1424', 'T1017', 'FW', 'Kisho', 'Yano', TO_DATE('1984-04-05', 'YYYY-MM-DD'), 18, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1425', 'T1017', 'DF', 'Daiki', 'Iwamasa', TO_DATE('1982-01-30', 'YYYY-MM-DD'), 2, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1426', 'T1017', 'MF', 'Kengo', 'Nakamura', TO_DATE('1980-10-31', 'YYYY-MM-DD'), 47, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1427', 'T1017', 'DF', 'Yasuyuki', 'Konno', TO_DATE('1983-01-25', 'YYYY-MM-DD'), 37, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1428', 'T1017', 'FW', 'Yoshito', 'Okubo', TO_DATE('1982-06-09', 'YYYY-MM-DD'), 49, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1429', 'T1017', 'MF', 'Makoto', 'Hasebe', TO_DATE('1984-01-18', 'YYYY-MM-DD'), 31, 'Japan', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1430', 'T1017', 'MF', 'Keisuke', 'Honda', TO_DATE('1986-06-13', 'YYYY-MM-DD'), 15, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1431', 'T1017', 'FW', 'Takayuki', 'Morimoto', TO_DATE('1988-05-07', 'YYYY-MM-DD'), 6, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1432', 'T1017', 'MF', 'Junichi', 'Inamoto', TO_DATE('1979-09-18', 'YYYY-MM-DD'), 80, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1433', 'T1017', 'GK', 'Eiji', 'Kawashima', TO_DATE('1983-03-20', 'YYYY-MM-DD'), 10, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1434', 'T1017', 'DF', 'Yuji', 'Nakazawa', TO_DATE('1978-02-25', 'YYYY-MM-DD'), 11, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1435', 'T1017', 'GK', 'Yoshikatsu', 'Kawaguchi', TO_DATE('1975-08-15', 'YYYY-MM-DD'), 16, 'Japan', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1436', 'T1019', 'GK', 'Carlos', 'Kameni', TO_DATE('1984-02-18', 'YYYY-MM-DD'), 58, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1437', 'T1019', 'DF', 'Beno�t', 'Assou-Ekotto', TO_DATE('1984-03-24', 'YYYY-MM-DD'), 4, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1438', 'T1019', 'DF', 'Nicolas', 'N`Koulou', TO_DATE('1990-03-27', 'YYYY-MM-DD'), 6, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1439', 'T1019', 'DF', 'Rigobert', 'Song', TO_DATE('1976-07-01', 'YYYY-MM-DD'), 13, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1440', 'T1019', 'DF', 'S�bastien', 'Bassong', TO_DATE('1986-07-09', 'YYYY-MM-DD'), 3, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1441', 'T1019', 'MF', 'Alex', 'Song', TO_DATE('1987-09-09', 'YYYY-MM-DD'), 20, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1442', 'T1019', 'MF', 'Landry', 'N`Gu�mo', TO_DATE('1985-11-28', 'YYYY-MM-DD'), 17, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1443', 'T1019', 'DF', 'Geremi', NULL, TO_DATE('1978-12-20', 'YYYY-MM-DD'), 10, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1444', 'T1019', 'FW', 'Samuel', 'Eto`o', TO_DATE('1981-03-10', 'YYYY-MM-DD'), 92, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1445', 'T1019', 'MF', 'Achille', 'Emana', TO_DATE('1982-06-05', 'YYYY-MM-DD'), 32, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1446', 'T1019', 'MF', 'Jean', 'Makoun', TO_DATE('1983-05-29', 'YYYY-MM-DD'), 46, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1447', 'T1019', 'DF', 'Ga�tan', 'Bong', TO_DATE('1988-04-25', 'YYYY-MM-DD'), 7, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1448', 'T1019', 'FW', 'Eric', 'Choupo-Moting', TO_DATE('1989-03-23', 'YYYY-MM-DD'), 9, 'Cameroon', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1449', 'T1019', 'DF', 'Aur�lien', 'Chedjou', TO_DATE('1985-06-20', 'YYYY-MM-DD'), 8, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1450', 'T1019', 'FW', 'Pierre', 'Web�', TO_DATE('1982-01-20', 'YYYY-MM-DD'), 39, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1451', 'T1019', 'GK', 'Souleymanou', 'Hamidou', TO_DATE('1973-11-22', 'YYYY-MM-DD'), 40, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1452', 'T1019', 'FW', 'Mohammadou', 'Idrissou', TO_DATE('1980-03-08', 'YYYY-MM-DD'), 28, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1453', 'T1019', 'MF', 'Eyong', 'Enoh', TO_DATE('1986-03-23', 'YYYY-MM-DD'), 12, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1454', 'T1019', 'DF', 'St�phane', 'Mbia', TO_DATE('1986-05-20', 'YYYY-MM-DD'), 29, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1455', 'T1019', 'MF', 'Georges', 'Mandjeck', TO_DATE('1988-12-09', 'YYYY-MM-DD'), 4, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1456', 'T1019', 'MF', 'Jo�l', 'Matip', TO_DATE('1991-08-08', 'YYYY-MM-DD'), 1, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1457', 'T1019', 'GK', 'Guy', 'N`dy-Assemb�', TO_DATE('1986-02-28', 'YYYY-MM-DD'), 30, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1458', 'T1019', 'FW', 'Vincent', 'Aboubakar', TO_DATE('1992-01-22', 'YYYY-MM-DD'), 20, 'Cameroon', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1459', 'T1023', 'GK', 'Gianluigi', 'Buffon', TO_DATE('1978-01-28', 'YYYY-MM-DD'), 1, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1460', 'T1023', 'DF', 'Christian', 'Maggio', TO_DATE('1982-02-11', 'YYYY-MM-DD'), 5, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1461', 'T1023', 'DF', 'Domenico', 'Criscito', TO_DATE('1986-12-30', 'YYYY-MM-DD'), 7, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1462', 'T1023', 'DF', 'Giorgio', 'Chiellini', TO_DATE('1984-08-14', 'YYYY-MM-DD'), 29, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1463', 'T1023', 'DF', 'Fabio', 'Cannavaro', TO_DATE('1973-09-13', 'YYYY-MM-DD'), 33, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1464', 'T1023', 'MF', 'Daniele', 'De-Rossi', TO_DATE('1983-07-24', 'YYYY-MM-DD'), 54, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1465', 'T1023', 'MF', 'Simone', 'Pepe', TO_DATE('1983-08-30', 'YYYY-MM-DD'), 15, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1466', 'T1023', 'MF', 'Gennaro', 'Gattuso', TO_DATE('1978-01-09', 'YYYY-MM-DD'), 72, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1467', 'T1023', 'FW', 'Vincenzo', 'Iaquinta', TO_DATE('1979-11-21', 'YYYY-MM-DD'), 37, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1468', 'T1023', 'FW', 'Antonio', 'Di-Natale', TO_DATE('1977-10-13', 'YYYY-MM-DD'), 33, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1469', 'T1023', 'FW', 'Alberto', 'Gilardino', TO_DATE('1982-07-05', 'YYYY-MM-DD'), 41, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1470', 'T1023', 'GK', 'Federico', 'Marchetti', TO_DATE('1983-02-07', 'YYYY-MM-DD'), 5, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1471', 'T1023', 'DF', 'Salvatore', 'Bocchetti', TO_DATE('1986-11-30', 'YYYY-MM-DD'), 13, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1472', 'T1023', 'GK', 'Morgan', 'De-Sanctis', TO_DATE('1977-03-26', 'YYYY-MM-DD'), 3, 'Italy', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1473', 'T1023', 'MF', 'Claudio', 'Marchisio', TO_DATE('1986-01-19', 'YYYY-MM-DD'), 4, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1474', 'T1023', 'MF', 'Mauro', 'Camoranesi', TO_DATE('1976-10-04', 'YYYY-MM-DD'), 53, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1475', 'T1023', 'MF', 'Angelo', 'Palombo', TO_DATE('1981-09-25', 'YYYY-MM-DD'), 17, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1476', 'T1023', 'FW', 'Fabio', 'Quagliarella', TO_DATE('1983-01-31', 'YYYY-MM-DD'), 20, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1477', 'T1023', 'DF', 'Gianluca', 'Zambrotta', TO_DATE('1977-02-19', 'YYYY-MM-DD'), 94, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1478', 'T1023', 'FW', 'Giampaolo', 'Pazzini', TO_DATE('1984-08-02', 'YYYY-MM-DD'), 8, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1479', 'T1023', 'MF', 'Andrea', 'Pirlo', TO_DATE('1979-05-19', 'YYYY-MM-DD'), 66, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1480', 'T1023', 'MF', 'Riccardo', 'Montolivo', TO_DATE('1985-01-18', 'YYYY-MM-DD'), 13, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1481', 'T1023', 'DF', 'Leonardo', 'Bonucci', TO_DATE('1987-05-01', 'YYYY-MM-DD'), 2, 'Italy', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1482', 'T1020', 'GK', 'Justo', 'Villar', TO_DATE('1977-06-30', 'YYYY-MM-DD'), 71, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1483', 'T1020', 'DF', 'Dar�o', 'Ver�n', TO_DATE('1979-06-26', 'YYYY-MM-DD'), 27, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1484', 'T1020', 'DF', 'Claudio', 'Morel', TO_DATE('1978-02-02', 'YYYY-MM-DD'), 25, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1485', 'T1020', 'DF', 'Denis', 'Caniza', TO_DATE('1974-08-29', 'YYYY-MM-DD'), 95, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1486', 'T1020', 'DF', 'Julio', 'C�sar-C�ceres', TO_DATE('1979-10-05', 'YYYY-MM-DD'), 59, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1487', 'T1020', 'DF', 'Carlos', 'Bonet', TO_DATE('1977-10-02', 'YYYY-MM-DD'), 60, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1488', 'T1020', 'FW', '�scar', 'Cardozo', TO_DATE('1983-05-20', 'YYYY-MM-DD'), 29, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1489', 'T1020', 'MF', '�dgar', 'Barreto', TO_DATE('1984-07-15', 'YYYY-MM-DD'), 47, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1490', 'T1020', 'FW', 'Roque', 'Santa-Cruz', TO_DATE('1981-08-16', 'YYYY-MM-DD'), 6, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1491', 'T1020', 'FW', '�dgar', 'Ben�tez', TO_DATE('1987-11-08', 'YYYY-MM-DD'), 12, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1492', 'T1020', 'MF', 'Jonathan', 'Santana', TO_DATE('1981-10-19', 'YYYY-MM-DD'), 21, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1493', 'T1020', 'GK', 'Diego', 'Barreto', TO_DATE('1981-07-16', 'YYYY-MM-DD'), 2, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1494', 'T1020', 'MF', 'Enrique', 'Vera', TO_DATE('1979-03-10', 'YYYY-MM-DD'), 28, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1495', 'T1020', 'DF', 'Paulo', 'Da-Silva', TO_DATE('1980-02-01', 'YYYY-MM-DD'), 67, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1496', 'T1020', 'MF', 'V�ctor', 'C�ceres', TO_DATE('1985-03-25', 'YYYY-MM-DD'), 13, 'Paraguay', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1497', 'T1020', 'MF', 'Cristian', 'Riveros', TO_DATE('1982-10-16', 'YYYY-MM-DD'), 45, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1498', 'T1020', 'DF', 'Aureliano', 'Torres', TO_DATE('1982-06-16', 'YYYY-MM-DD'), 25, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1499', 'T1020', 'FW', 'Nelson', 'Valdez', TO_DATE('1983-11-28', 'YYYY-MM-DD'), 38, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1500', 'T1020', 'FW', 'Lucas', 'Barrios', TO_DATE('1984-11-13', 'YYYY-MM-DD'), 3, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1501', 'T1020', 'MF', 'N�stor', 'Ortigoza', TO_DATE('1984-10-07', 'YYYY-MM-DD'), 33, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1502', 'T1020', 'DF', 'Antol�n', 'Alcaraz', TO_DATE('1982-07-30', 'YYYY-MM-DD'), 5, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1503', 'T1020', 'GK', 'Aldo', 'Bobadilla', TO_DATE('1976-04-20', 'YYYY-MM-DD'), 18, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1504', 'T1020', 'FW', 'Rodolfo', 'Gamarra', TO_DATE('1988-12-10', 'YYYY-MM-DD'), 2, 'Paraguay', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1505', 'T1022', 'GK', 'Mark', 'Paston', TO_DATE('1976-12-13', 'YYYY-MM-DD'), 23, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1506', 'T1022', 'DF', 'Ben', 'Sigmund', TO_DATE('1981-02-03', 'YYYY-MM-DD'), 14, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1507', 'T1022', 'DF', 'Tony', 'Lochhead', TO_DATE('1982-01-12', 'YYYY-MM-DD'), 30, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1508', 'T1022', 'DF', 'Winston', 'Reid', TO_DATE('1988-07-03', 'YYYY-MM-DD'), 3, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1509', 'T1022', 'DF', 'Ivan', 'Vicelich', TO_DATE('1976-09-03', 'YYYY-MM-DD'), 66, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1510', 'T1022', 'DF', 'Ryan', 'Nelsen', TO_DATE('1977-10-18', 'YYYY-MM-DD'), 41, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1511', 'T1022', 'MF', 'Simon', 'Elliott', TO_DATE('1974-06-10', 'YYYY-MM-DD'), 63, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1512', 'T1022', 'MF', 'Tim', 'Brown', TO_DATE('1981-03-06', 'YYYY-MM-DD'), 25, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1513', 'T1022', 'FW', 'Shane', 'Smeltz', TO_DATE('1981-09-29', 'YYYY-MM-DD'), 33, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1514', 'T1022', 'FW', 'Chris', 'Killen', TO_DATE('1981-10-08', 'YYYY-MM-DD'), 31, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1515', 'T1022', 'MF', 'Leo', 'Bertos', TO_DATE('1981-12-20', 'YYYY-MM-DD'), 34, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1516', 'T1022', 'GK', 'Glen', 'Moss', TO_DATE('1983-01-19', 'YYYY-MM-DD'), 15, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1517', 'T1022', 'MF', 'Andy', 'Barron', TO_DATE('1980-12-24', 'YYYY-MM-DD'), 11, 'New Zealand', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1518', 'T1022', 'FW', 'Rory', 'Fallon', TO_DATE('1982-03-20', 'YYYY-MM-DD'), 7, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1519', 'T1022', 'MF', 'Michael', 'McGlinchey', TO_DATE('1987-01-07', 'YYYY-MM-DD'), 5, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1520', 'T1022', 'MF', 'Aaron', 'Clapham', TO_DATE('1987-01-01', 'YYYY-MM-DD'), 20, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1521', 'T1022', 'MF', 'David', 'Mulligan', TO_DATE('1982-03-24', 'YYYY-MM-DD'), 25, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1522', 'T1022', 'DF', 'Andrew', 'Boyens', TO_DATE('1983-09-18', 'YYYY-MM-DD'), 15, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1523', 'T1022', 'DF', 'Tommy', 'Smith', TO_DATE('1990-03-31', 'YYYY-MM-DD'), 4, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1524', 'T1022', 'FW', 'Chris', 'Wood', TO_DATE('1991-12-09', 'YYYY-MM-DD'), 9, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1525', 'T1022', 'MF', 'Jeremy', 'Christie', TO_DATE('1983-05-22', 'YYYY-MM-DD'), 22, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1526', 'T1022', 'MF', 'Jeremy', 'Brockie', TO_DATE('1987-10-07', 'YYYY-MM-DD'), 18, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1527', 'T1022', 'GK', 'James', 'Bannatyne', TO_DATE('1975-06-30', 'YYYY-MM-DD'), 3, 'New Zealand', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1528', 'T1021', 'GK', 'J�n', 'Mucha', TO_DATE('1982-12-05', 'YYYY-MM-DD'), 14, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1529', 'T1021', 'DF', 'Peter', 'Pekar�k', TO_DATE('1986-10-30', 'YYYY-MM-DD'), 21, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1530', 'T1021', 'DF', 'Martin', '�krtel', TO_DATE('1984-12-15', 'YYYY-MM-DD'), 37, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1531', 'T1021', 'DF', 'Marek', 'Cech', TO_DATE('1983-01-26', 'YYYY-MM-DD'), 38, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1532', 'T1021', 'DF', 'Radoslav', 'Zabavn�k', TO_DATE('1980-09-16', 'YYYY-MM-DD'), 42, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1533', 'T1021', 'MF', 'Zdeno', '�trba', TO_DATE('1976-06-09', 'YYYY-MM-DD'), 20, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1534', 'T1021', 'MF', 'J�n', 'Koz�k', TO_DATE('1980-04-22', 'YYYY-MM-DD'), 22, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1535', 'T1021', 'MF', 'Stanislav', '�est�k', TO_DATE('1982-12-16', 'YYYY-MM-DD'), 29, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1536', 'T1021', 'MF', 'Marek', 'Sapara', TO_DATE('1982-07-31', 'YYYY-MM-DD'), 24, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1537', 'T1021', 'FW', 'R�bert', 'Vittek', TO_DATE('1982-04-01', 'YYYY-MM-DD'), 69, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1538', 'T1021', 'GK', 'Du�an', 'Perni�', TO_DATE('1984-11-28', 'YYYY-MM-DD'), 1, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1539', 'T1021', 'FW', 'Filip', 'Holo�ko', TO_DATE('1984-01-17', 'YYYY-MM-DD'), 37, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1540', 'T1021', 'FW', 'Martin', 'Jakubko', TO_DATE('1980-02-26', 'YYYY-MM-DD'), 21, 'Slovakia', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1541', 'T1021', 'MF', 'Miroslav', 'Stoch', TO_DATE('1989-10-19', 'YYYY-MM-DD'), 10, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1542', 'T1021', 'DF', 'J�n', 'Durica', TO_DATE('1981-12-10', 'YYYY-MM-DD'), 35, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1543', 'T1021', 'MF', 'Marek', 'Ham��k', TO_DATE('1987-07-27', 'YYYY-MM-DD'), 30, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1544', 'T1021', 'FW', 'Erik', 'Jendri�ek', TO_DATE('1986-10-26', 'YYYY-MM-DD'), 13, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1545', 'T1021', 'MF', 'Juraj', 'Kucka', TO_DATE('1987-02-26', 'YYYY-MM-DD'), 5, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1546', 'T1021', 'MF', 'Kamil', 'Kop�nek', TO_DATE('1984-05-18', 'YYYY-MM-DD'), 7, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1547', 'T1021', 'DF', 'Kornel', 'Sal�ta', TO_DATE('1985-01-04', 'YYYY-MM-DD'), 3, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1548', 'T1021', 'DF', 'Martin', 'Petr�', TO_DATE('1979-11-02', 'YYYY-MM-DD'), 38, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1549', 'T1021', 'GK', 'Du�an', 'Kuciak', TO_DATE('1985-05-21', 'YYYY-MM-DD'), 2, 'Slovakia', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1550', 'T1024', 'GK', 'J�lio', 'C�sar', TO_DATE('1979-09-03', 'YYYY-MM-DD'), 47, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1551', 'T1024', 'DF', 'Maicon', NULL, TO_DATE('1981-07-26', 'YYYY-MM-DD'), 56, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1552', 'T1024', 'DF', 'L�cio', NULL, TO_DATE('1978-05-08', 'YYYY-MM-DD'), 89, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1553', 'T1024', 'DF', 'Juan', NULL, TO_DATE('1979-02-01', 'YYYY-MM-DD'), 73, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1554', 'T1024', 'MF', 'Felipe', 'Melo', TO_DATE('1983-08-26', 'YYYY-MM-DD'), 16, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1555', 'T1024', 'DF', 'Michel', 'Bastos', TO_DATE('1983-08-02', 'YYYY-MM-DD'), 3, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1556', 'T1024', 'MF', 'Elano', NULL, TO_DATE('1981-06-14', 'YYYY-MM-DD'), 41, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1557', 'T1024', 'MF', 'Gilberto', 'Silva', TO_DATE('1976-10-07', 'YYYY-MM-DD'), 86, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1558', 'T1024', 'FW', 'Lu�s', 'Fabiano', TO_DATE('1980-11-08', 'YYYY-MM-DD'), 36, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1559', 'T1024', 'MF', 'Kak�', NULL, TO_DATE('1982-04-22', 'YYYY-MM-DD'), 76, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1560', 'T1024', 'FW', 'Robinho', NULL, TO_DATE('1984-01-25', 'YYYY-MM-DD'), 73, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1561', 'T1024', 'GK', 'Gomes', NULL, TO_DATE('1981-02-15', 'YYYY-MM-DD'), 9, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1562', 'T1024', 'DF', 'Daniel', 'Alves', TO_DATE('1983-05-06', 'YYYY-MM-DD'), 33, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1563', 'T1024', 'DF', 'Luis�o', NULL, TO_DATE('1981-02-13', 'YYYY-MM-DD'), 40, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1564', 'T1024', 'DF', 'Thiago', 'Silva', TO_DATE('1984-09-22', 'YYYY-MM-DD'), 4, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1565', 'T1024', 'DF', 'Gilberto', NULL, TO_DATE('1976-04-25', 'YYYY-MM-DD'), 32, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1566', 'T1024', 'MF', 'Josu�', NULL, TO_DATE('1979-07-19', 'YYYY-MM-DD'), 26, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1567', 'T1024', 'MF', 'Ramires', NULL, TO_DATE('1987-03-24', 'YYYY-MM-DD'), 11, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1568', 'T1024', 'MF', 'J�lio', 'Baptista', TO_DATE('1981-10-01', 'YYYY-MM-DD'), 45, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1569', 'T1024', 'MF', 'Kl�berson', NULL, TO_DATE('1979-06-19', 'YYYY-MM-DD'), 31, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1570', 'T1024', 'FW', 'Nilmar', NULL, TO_DATE('1984-07-14', 'YYYY-MM-DD'), 15, 'Brazil', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1571', 'T1024', 'GK', 'Doni', NULL, TO_DATE('1979-10-22', 'YYYY-MM-DD'), 10, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1572', 'T1024', 'FW', 'Grafite', NULL, TO_DATE('1979-04-02', 'YYYY-MM-DD'), 2, 'Brazil', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1573', 'T1027', 'GK', 'Ri', 'Myong-Guk', TO_DATE('1986-09-09', 'YYYY-MM-DD'), 28, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1574', 'T1027', 'DF', 'Cha', 'Jong-Hyok', TO_DATE('1985-09-25', 'YYYY-MM-DD'), 31, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1575', 'T1027', 'DF', 'Ri', 'Jun-Il', TO_DATE('1987-08-24', 'YYYY-MM-DD'), 26, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1576', 'T1027', 'MF', 'Pak', 'Nam-Chol', TO_DATE('1985-07-02', 'YYYY-MM-DD'), 35, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1577', 'T1027', 'DF', 'Ri', 'Kwang-Chon', TO_DATE('1985-09-04', 'YYYY-MM-DD'), 41, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1578', 'T1027', 'MF', 'Kim', 'Kum-Il', TO_DATE('1987-10-10', 'YYYY-MM-DD'), 11, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1579', 'T1027', 'FW', 'An', 'Chol-Hyok', TO_DATE('1985-06-27', 'YYYY-MM-DD'), 16, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1580', 'T1027', 'DF', 'Ji', 'Yun-Nam', TO_DATE('1976-11-20', 'YYYY-MM-DD'), 23, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1581', 'T1027', 'FW', 'Jong', 'Tae-Se', TO_DATE('1984-03-02', 'YYYY-MM-DD'), 20, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1582', 'T1027', 'FW', 'Hong', 'Yong-Jo', TO_DATE('1982-05-22', 'YYYY-MM-DD'), 40, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1583', 'T1027', 'MF', 'Mun', 'In-Guk', TO_DATE('1978-09-29', 'YYYY-MM-DD'), 42, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1584', 'T1027', 'FW', 'Choe', 'Kum-Chol', TO_DATE('1987-02-09', 'YYYY-MM-DD'), 16, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1585', 'T1027', 'DF', 'Pak', 'Chol-Jin', TO_DATE('1985-09-05', 'YYYY-MM-DD'), 34, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1586', 'T1027', 'DF', 'Pak', 'Nam-Chol', TO_DATE('1988-10-03', 'YYYY-MM-DD'), 12, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1587', 'T1027', 'MF', 'Kim', 'Yong-Jun', TO_DATE('1983-07-19', 'YYYY-MM-DD'), 52, 'Korea DPR', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1588', 'T1027', 'DF', 'Nam', 'Song-Chol', TO_DATE('1982-05-07', 'YYYY-MM-DD'), 41, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1589', 'T1027', 'MF', 'An', 'Yong-Hak', TO_DATE('1978-10-25', 'YYYY-MM-DD'), 24, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1590', 'T1027', 'GK', 'Kim', 'Myong-Gil', TO_DATE('1984-10-16', 'YYYY-MM-DD'), 10, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1591', 'T1027', 'MF', 'Ri', 'Chol-Myong', TO_DATE('1988-02-18', 'YYYY-MM-DD'), 11, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1592', 'T1027', 'GK', 'Kim', 'Myong-Won', TO_DATE('1983-07-15', 'YYYY-MM-DD'), 9, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1593', 'T1027', 'DF', 'Ri', 'Kwang-Hyok', TO_DATE('1987-08-17', 'YYYY-MM-DD'), 15, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1594', 'T1027', 'MF', 'Kim', 'Kyong-Il', TO_DATE('1988-12-11', 'YYYY-MM-DD'), 7, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1595', 'T1027', 'MF', 'Pak', 'Sung-Hyok', TO_DATE('1990-05-30', 'YYYY-MM-DD'), 3, 'Korea DPR', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1596', 'T1026', 'GK', 'Boubacar', 'Barry', TO_DATE('1979-12-30', 'YYYY-MM-DD'), 45, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1597', 'T1026', 'DF', 'Benjamin', 'Angoua', TO_DATE('1986-11-28', 'YYYY-MM-DD'), 7, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1598', 'T1026', 'DF', 'Arthur', 'Boka', TO_DATE('1983-04-02', 'YYYY-MM-DD'), 54, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1599', 'T1026', 'DF', 'Kolo', 'Tour�', TO_DATE('1981-03-19', 'YYYY-MM-DD'), 76, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1600', 'T1026', 'MF', 'Didier', 'Zokora', TO_DATE('1980-12-14', 'YYYY-MM-DD'), 80, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1601', 'T1026', 'DF', 'Steve', 'Gohouri', TO_DATE('1981-02-08', 'YYYY-MM-DD'), 11, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1602', 'T1026', 'FW', 'Seydou', 'Doumbia', TO_DATE('1987-12-31', 'YYYY-MM-DD'), 5, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1603', 'T1026', 'FW', 'Salomon', 'Kalou', TO_DATE('1985-08-05', 'YYYY-MM-DD'), 28, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1604', 'T1026', 'MF', 'Cheick', 'Tiot�', TO_DATE('1986-06-21', 'YYYY-MM-DD'), 8, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1605', 'T1026', 'FW', 'Gervinho', NULL, TO_DATE('1987-05-27', 'YYYY-MM-DD'), 15, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1606', 'T1026', 'FW', 'Didier', 'Drogba', TO_DATE('1978-03-11', 'YYYY-MM-DD'), 68, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1607', 'T1026', 'MF', 'Jean-Jacques', 'Gosso', TO_DATE('1983-03-15', 'YYYY-MM-DD'), 6, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1608', 'T1026', 'MF', 'Romaric', NULL, TO_DATE('1983-06-04', 'YYYY-MM-DD'), 38, 'Ivory Coast', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1609', 'T1026', 'MF', 'Emmanuel', 'Kon�', TO_DATE('1986-12-31', 'YYYY-MM-DD'), 12, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1610', 'T1026', 'FW', 'Aruna', 'Dindane', TO_DATE('1980-11-26', 'YYYY-MM-DD'), 54, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1611', 'T1026', 'GK', 'Aristide', 'Zogbo', TO_DATE('1981-12-30', 'YYYY-MM-DD'), 6, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1612', 'T1026', 'DF', 'Siaka', 'Ti�n�', TO_DATE('1982-03-22', 'YYYY-MM-DD'), 55, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1613', 'T1026', 'MF', 'Kader', 'Ke�ta', TO_DATE('1981-08-06', 'YYYY-MM-DD'), 55, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1614', 'T1026', 'MF', 'Yaya', 'Tour�', TO_DATE('1983-05-13', 'YYYY-MM-DD'), 47, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1615', 'T1026', 'DF', 'Guy', 'Demel', TO_DATE('1981-06-13', 'YYYY-MM-DD'), 26, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1616', 'T1026', 'DF', 'Emmanuel', 'Ebou�', TO_DATE('1983-06-04', 'YYYY-MM-DD'), 52, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1617', 'T1026', 'DF', 'Sol', 'Bamba', TO_DATE('1985-01-13', 'YYYY-MM-DD'), 16, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1618', 'T1026', 'GK', 'Daniel', 'Yeboah', TO_DATE('1984-11-13', 'YYYY-MM-DD'), 4, 'Ivory Coast', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1619', 'T1025', 'GK', 'Eduardo', NULL, TO_DATE('1982-09-19', 'YYYY-MM-DD'), 12, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1620', 'T1025', 'DF', 'Bruno', 'Alves', TO_DATE('1981-11-27', 'YYYY-MM-DD'), 28, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1621', 'T1025', 'DF', 'Paulo', 'Ferreira', TO_DATE('1979-01-18', 'YYYY-MM-DD'), 59, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1622', 'T1025', 'DF', 'Rolando', NULL, TO_DATE('1985-08-31', 'YYYY-MM-DD'), 7, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1623', 'T1025', 'DF', 'Duda', NULL, TO_DATE('1980-06-27', 'YYYY-MM-DD'), 14, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1624', 'T1025', 'DF', 'Ricardo', 'Carvalho', TO_DATE('1978-05-18', 'YYYY-MM-DD'), 60, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1625', 'T1025', 'FW', 'Cristiano', 'Ronaldo', TO_DATE('1985-02-05', 'YYYY-MM-DD'), 69, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1626', 'T1025', 'MF', 'Pedro', 'Mendes', TO_DATE('1979-02-26', 'YYYY-MM-DD'), 5, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1627', 'T1025', 'FW', 'Li�dson', NULL, TO_DATE('1977-12-17', 'YYYY-MM-DD'), 7, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1628', 'T1025', 'MF', 'Danny', NULL, TO_DATE('1983-08-07', 'YYYY-MM-DD'), 8, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1629', 'T1025', 'MF', 'Sim�o', NULL, TO_DATE('1979-10-31', 'YYYY-MM-DD'), 79, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1630', 'T1025', 'GK', 'Beto', NULL, TO_DATE('1982-05-01', 'YYYY-MM-DD'), 1, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1631', 'T1025', 'DF', 'Miguel', NULL, TO_DATE('1980-01-04', 'YYYY-MM-DD'), 53, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1632', 'T1025', 'MF', 'Miguel', 'Veloso', TO_DATE('1986-05-11', 'YYYY-MM-DD'), 10, 'Portugal', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1633', 'T1025', 'DF', 'Pepe', NULL, TO_DATE('1983-02-26', 'YYYY-MM-DD'), 24, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1634', 'T1025', 'MF', 'Raul', 'Meireles', TO_DATE('1983-03-17', 'YYYY-MM-DD'), 31, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1635', 'T1025', 'MF', 'R�ben', 'Amorim', TO_DATE('1985-01-27', 'YYYY-MM-DD'), 0, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1636', 'T1025', 'FW', 'Hugo', 'Almeida', TO_DATE('1984-05-23', 'YYYY-MM-DD'), 23, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1637', 'T1025', 'MF', 'Tiago', NULL, TO_DATE('1981-05-02', 'YYYY-MM-DD'), 49, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1638', 'T1025', 'MF', 'Deco', NULL, TO_DATE('1977-08-27', 'YYYY-MM-DD'), 71, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1639', 'T1025', 'DF', 'Ricardo', 'Costa', TO_DATE('1981-05-16', 'YYYY-MM-DD'), 6, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1640', 'T1025', 'GK', 'Daniel', 'Fernandes', TO_DATE('1983-09-25', 'YYYY-MM-DD'), 2, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1641', 'T1025', 'DF', 'F�bio', 'Coentr�o', TO_DATE('1988-03-11', 'YYYY-MM-DD'), 3, 'Portugal', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1642', 'T1028', 'GK', 'Iker', 'Casillas', TO_DATE('1981-05-20', 'YYYY-MM-DD'), 11, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1643', 'T1028', 'DF', 'Ra�l', 'Albiol', TO_DATE('1985-09-04', 'YYYY-MM-DD'), 23, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1644', 'T1028', 'DF', 'Gerard', 'Piqu�', TO_DATE('1987-02-02', 'YYYY-MM-DD'), 16, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1645', 'T1028', 'DF', 'Carlos', 'Marchena', TO_DATE('1979-07-31', 'YYYY-MM-DD'), 59, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1646', 'T1028', 'DF', 'Carles', 'Puyol', TO_DATE('1978-04-13', 'YYYY-MM-DD'), 83, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1647', 'T1028', 'MF', 'Andr�s', 'Iniesta', TO_DATE('1984-05-11', 'YYYY-MM-DD'), 43, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1648', 'T1028', 'FW', 'David', 'Villa', TO_DATE('1981-12-03', 'YYYY-MM-DD'), 58, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1649', 'T1028', 'MF', 'Xavi', NULL, TO_DATE('1980-01-25', 'YYYY-MM-DD'), 87, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1650', 'T1028', 'FW', 'Fernando', 'Torres', TO_DATE('1984-03-20', 'YYYY-MM-DD'), 73, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1651', 'T1028', 'MF', 'Cesc', 'F�bregas', TO_DATE('1987-05-04', 'YYYY-MM-DD'), 49, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1652', 'T1028', 'DF', 'Joan', 'Capdevila', TO_DATE('1978-02-03', 'YYYY-MM-DD'), 46, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1653', 'T1028', 'GK', 'V�ctor', 'Vald�s', TO_DATE('1982-01-14', 'YYYY-MM-DD'), 1, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1654', 'T1028', 'MF', 'Juan', 'Mata', TO_DATE('1988-04-28', 'YYYY-MM-DD'), 8, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1655', 'T1028', 'MF', 'Xabi', 'Alonso', TO_DATE('1981-11-25', 'YYYY-MM-DD'), 69, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1656', 'T1028', 'DF', 'Sergio', 'Ramos', TO_DATE('1986-03-30', 'YYYY-MM-DD'), 60, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1657', 'T1028', 'MF', 'Sergio', 'Busquets', TO_DATE('1988-07-16', 'YYYY-MM-DD'), 13, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1658', 'T1028', 'DF', '�lvaro', 'Arbeloa', TO_DATE('1983-01-17', 'YYYY-MM-DD'), 15, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1659', 'T1028', 'FW', 'Pedro', NULL, TO_DATE('1987-07-28', 'YYYY-MM-DD'), 3, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1660', 'T1028', 'FW', 'Fernando', 'Llorente', TO_DATE('1985-02-26', 'YYYY-MM-DD'), 7, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1661', 'T1028', 'MF', 'Javi', 'Mart�nez', TO_DATE('1988-09-02', 'YYYY-MM-DD'), 2, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1662', 'T1028', 'MF', 'David', 'Silva', TO_DATE('1986-01-08', 'YYYY-MM-DD'), 36, 'Spain', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1663', 'T1028', 'MF', 'Jes�s', 'Navas', TO_DATE('1985-11-21', 'YYYY-MM-DD'), 6, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1664', 'T1028', 'GK', 'Pepe', 'Reina', TO_DATE('1982-08-31', 'YYYY-MM-DD'), 20, 'Spain', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1665', 'T1030', 'GK', 'Diego', 'Benaglio', TO_DATE('1983-09-08', 'YYYY-MM-DD'), 25, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1666', 'T1030', 'DF', 'Stephan', 'Lichtsteiner', TO_DATE('1984-01-16', 'YYYY-MM-DD'), 26, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1667', 'T1030', 'DF', 'Ludovic', 'Magnin', TO_DATE('1979-04-20', 'YYYY-MM-DD'), 61, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1668', 'T1030', 'DF', 'Philippe', 'Senderos', TO_DATE('1985-02-14', 'YYYY-MM-DD'), 38, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1669', 'T1030', 'DF', 'Steve', 'von-Bergen', TO_DATE('1983-06-10', 'YYYY-MM-DD'), 10, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1670', 'T1030', 'MF', 'Benjamin', 'Huggel', TO_DATE('1977-07-07', 'YYYY-MM-DD'), 36, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1671', 'T1030', 'MF', 'Tranquillo', 'Barnetta', TO_DATE('1985-05-22', 'YYYY-MM-DD'), 50, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1672', 'T1030', 'MF', 'G�khan', 'Inler', TO_DATE('1984-06-27', 'YYYY-MM-DD'), 34, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1673', 'T1030', 'FW', 'Alexander', 'Frei', TO_DATE('1979-07-15', 'YYYY-MM-DD'), 73, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1674', 'T1030', 'FW', 'Blaise', 'Nkufo', TO_DATE('1975-05-25', 'YYYY-MM-DD'), 29, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1675', 'T1030', 'MF', 'Valon', 'Behrami', TO_DATE('1985-04-19', 'YYYY-MM-DD'), 26, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1676', 'T1030', 'GK', 'Marco', 'W�lfli', TO_DATE('1982-08-22', 'YYYY-MM-DD'), 4, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1677', 'T1030', 'DF', 'St�phane', 'Grichting', TO_DATE('1979-03-30', 'YYYY-MM-DD'), 33, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1678', 'T1030', 'MF', 'Marco', 'Padalino', TO_DATE('1983-12-08', 'YYYY-MM-DD'), 7, 'Switzerland', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1679', 'T1030', 'MF', 'Hakan', 'Yakin', TO_DATE('1977-02-22', 'YYYY-MM-DD'), 80, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1680', 'T1030', 'MF', 'Gelson', 'Fernandes', TO_DATE('1986-09-02', 'YYYY-MM-DD'), 22, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1681', 'T1030', 'DF', 'Reto', 'Ziegler', TO_DATE('1986-01-16', 'YYYY-MM-DD'), 10, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1682', 'T1030', 'FW', 'Albert', 'Bunjaku', TO_DATE('1983-11-29', 'YYYY-MM-DD'), 1, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1683', 'T1030', 'FW', 'Eren', 'Derdiyok', TO_DATE('1988-06-12', 'YYYY-MM-DD'), 19, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1684', 'T1030', 'MF', 'Pirmin', 'Schwegler', TO_DATE('1987-03-09', 'YYYY-MM-DD'), 3, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1685', 'T1030', 'GK', 'Johnny', 'Leoni', TO_DATE('1984-06-30', 'YYYY-MM-DD'), 10, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1686', 'T1030', 'DF', 'Mario', 'Eggimann', TO_DATE('1981-01-24', 'YYYY-MM-DD'), 8, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1687', 'T1030', 'MF', 'Xherdan', 'Shaqiri', TO_DATE('1991-10-10', 'YYYY-MM-DD'), 1, 'Switzerland', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1688', 'T1031', 'GK', 'Ricardo', 'Canales', TO_DATE('1982-05-30', 'YYYY-MM-DD'), 2, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1689', 'T1031', 'DF', 'Osman', 'Ch�vez', TO_DATE('1984-07-29', 'YYYY-MM-DD'), 26, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1690', 'T1031', 'DF', 'Maynor', 'Figueroa', TO_DATE('1983-05-02', 'YYYY-MM-DD'), 66, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1691', 'T1031', 'DF', 'Johnny', 'Palacios', TO_DATE('1986-12-20', 'YYYY-MM-DD'), 4, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1692', 'T1031', 'DF', 'V�ctor', 'Bern�rdez', TO_DATE('1982-05-24', 'YYYY-MM-DD'), 40, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1693', 'T1031', 'MF', 'Hendry', 'Thomas', TO_DATE('1985-02-23', 'YYYY-MM-DD'), 39, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1694', 'T1031', 'MF', 'Ram�n', 'N��ez', TO_DATE('1984-11-14', 'YYYY-MM-DD'), 16, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1695', 'T1031', 'MF', 'Wilson', 'Palacios', TO_DATE('1984-07-29', 'YYYY-MM-DD'), 69, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1696', 'T1031', 'FW', 'Carlos', 'Pav�n', TO_DATE('1973-10-19', 'YYYY-MM-DD'), 98, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1697', 'T1031', 'FW', 'Jerry', 'Palacios', TO_DATE('1982-05-13', 'YYYY-MM-DD'), 11, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1698', 'T1031', 'FW', 'David', 'Suazo', TO_DATE('1979-11-05', 'YYYY-MM-DD'), 50, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1699', 'T1031', 'FW', 'Georgie', 'Welcome', TO_DATE('1985-03-09', 'YYYY-MM-DD'), 11, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1700', 'T1031', 'FW', 'Roger', 'Espinoza', TO_DATE('1986-10-25', 'YYYY-MM-DD'), 10, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1701', 'T1031', 'DF', 'Oscar', 'Garc�a', TO_DATE('1984-09-04', 'YYYY-MM-DD'), 42, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1702', 'T1031', 'FW', 'Walter', 'Mart�nez', TO_DATE('1982-03-29', 'YYYY-MM-DD'), 34, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1703', 'T1031', 'DF', 'Mauricio', 'Sabill�n', TO_DATE('1978-11-11', 'YYYY-MM-DD'), 25, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1704', 'T1031', 'MF', 'Edgar', '�lvarez', TO_DATE('1980-01-09', 'YYYY-MM-DD'), 46, 'Honduras', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1705', 'T1031', 'GK', 'Noel', 'Valladares', TO_DATE('1977-05-03', 'YYYY-MM-DD'), 71, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1706', 'T1031', 'MF', 'Danilo', 'Turcios', TO_DATE('1978-05-08', 'YYYY-MM-DD'), 82, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1707', 'T1031', 'MF', 'Amado', 'Guevara', TO_DATE('1976-05-02', 'YYYY-MM-DD'), 13, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1708', 'T1031', 'DF', 'Emilio', 'Izaguirre', TO_DATE('1986-05-10', 'YYYY-MM-DD'), 39, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1709', 'T1031', 'GK', 'Donis', 'Escober', TO_DATE('1980-02-03', 'YYYY-MM-DD'), 11, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1710', 'T1031', 'DF', 'Sergio', 'Mendoza', TO_DATE('1981-05-23', 'YYYY-MM-DD'), 46, 'Honduras', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1711', 'T1029', 'GK', 'Claudio', 'Bravo', TO_DATE('1983-04-13', 'YYYY-MM-DD'), 41, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1712', 'T1029', 'DF', 'Ismael', 'Fuentes', TO_DATE('1981-08-04', 'YYYY-MM-DD'), 25, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1713', 'T1029', 'DF', 'Waldo', 'Ponce', TO_DATE('1982-12-04', 'YYYY-MM-DD'), 23, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1714', 'T1029', 'MF', 'Mauricio', 'Isla', TO_DATE('1988-06-12', 'YYYY-MM-DD'), 10, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1715', 'T1029', 'DF', 'Pablo', 'Contreras', TO_DATE('1978-09-11', 'YYYY-MM-DD'), 49, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1716', 'T1029', 'MF', 'Carlos', 'Carmona', TO_DATE('1987-02-21', 'YYYY-MM-DD'), 18, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1717', 'T1029', 'FW', 'Alexis', 'S�nchez', TO_DATE('1988-12-19', 'YYYY-MM-DD'), 26, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1718', 'T1029', 'DF', 'Arturo', 'Vidal', TO_DATE('1987-05-22', 'YYYY-MM-DD'), 21, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1719', 'T1029', 'FW', 'Humberto', 'Suazo', TO_DATE('1981-05-10', 'YYYY-MM-DD'), 41, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1720', 'T1029', 'MF', 'Jorge', 'Valdivia', TO_DATE('1983-10-19', 'YYYY-MM-DD'), 36, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1721', 'T1029', 'FW', 'Mark', 'Gonz�lez', TO_DATE('1984-07-10', 'YYYY-MM-DD'), 38, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1722', 'T1029', 'GK', 'Miguel', 'Pinto', TO_DATE('1983-07-04', 'YYYY-MM-DD'), 13, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1723', 'T1029', 'MF', 'Marco', 'Estrada', TO_DATE('1983-05-28', 'YYYY-MM-DD'), 20, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1724', 'T1029', 'MF', 'Mat�as', 'Fern�ndez', TO_DATE('1986-05-15', 'YYYY-MM-DD'), 35, 'Chile', 1);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1725', 'T1029', 'FW', 'Jean', 'Beausejour', TO_DATE('1984-06-01', 'YYYY-MM-DD'), 23, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1726', 'T1029', 'FW', 'Fabi�n', 'Orellana', TO_DATE('1986-01-27', 'YYYY-MM-DD'), 13, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1727', 'T1029', 'DF', 'Gary', 'Medel', TO_DATE('1987-08-03', 'YYYY-MM-DD'), 23, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1728', 'T1029', 'DF', 'Gonzalo', 'Jara', TO_DATE('1985-08-29', 'YYYY-MM-DD'), 31, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1729', 'T1029', 'MF', 'Gonzalo', 'Fierro', TO_DATE('1983-03-21', 'YYYY-MM-DD'), 16, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1730', 'T1029', 'MF', 'Rodrigo', 'Millar', TO_DATE('1981-11-03', 'YYYY-MM-DD'), 19, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1731', 'T1029', 'MF', 'Rodrigo', 'Tello', TO_DATE('1979-10-14', 'YYYY-MM-DD'), 32, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1732', 'T1029', 'FW', 'Esteban', 'Paredes', TO_DATE('1980-08-01', 'YYYY-MM-DD'), 12, 'Chile', 0);
INSERT INTO PLAYERS (ID_player, ID_team, Pos, First_name, Last_name, Date_of_birth, Shirt_number, Nationality, Lineup) VALUES ('P1733', 'T1029', 'GK', 'Luis', 'Mar�n', TO_DATE('1983-05-18', 'YYYY-MM-DD'), 2, 'Chile', 0);

INSERT INTO REGIONS VALUES ('R1000', 'Africa');
INSERT INTO REGIONS VALUES ('R1001', 'Asia');
INSERT INTO REGIONS VALUES ('R1002', 'Europe');
INSERT INTO REGIONS VALUES ('R1003', 'North, Central America and Caribbean');
INSERT INTO REGIONS VALUES ('R1004', 'Oceania');
INSERT INTO REGIONS VALUES ('R1005', 'South America');
INSERT INTO REGIONS VALUES ('R1006', 'Worldwide');

INSERT INTO STADIUMS VALUES ('S1000', 'Soccer City');
INSERT INTO STADIUMS VALUES ('S1001', 'Green Point Stadium');
INSERT INTO STADIUMS VALUES ('S1002', 'Moses Mabhida Stadium');
INSERT INTO STADIUMS VALUES ('S1003', 'Nelson Mandela Bay Stadium');
INSERT INTO STADIUMS VALUES ('S1004', 'Ellis Park Stadium');
INSERT INTO STADIUMS VALUES ('S1005', 'Loftus Versfeld Stadium');
INSERT INTO STADIUMS VALUES ('S1006', 'Free State Stadium');
INSERT INTO STADIUMS VALUES ('S1007', 'Royal Bafokeng Stadium');
INSERT INTO STADIUMS VALUES ('S1008', 'Peter Mokaba Stadium');
INSERT INTO STADIUMS VALUES ('S1009', 'Mbombela Stadium');

INSERT INTO TEAM_STATUS VALUES ('T1000', 3, 2, 1, 0, 4, 0, 7);
INSERT INTO TEAM_STATUS VALUES ('T1001', 3, 1, 1, 1, 3, 2, 4);
INSERT INTO TEAM_STATUS VALUES ('T1002', 3, 1, 1, 1, 3, 5, 4);
INSERT INTO TEAM_STATUS VALUES ('T1003', 3, 0, 1, 2, 1, 4, 1);
INSERT INTO TEAM_STATUS VALUES ('T1004', 3, 3, 0, 0, 7, 1, 9);
INSERT INTO TEAM_STATUS VALUES ('T1005', 3, 1, 1, 1, 5, 6, 4);
INSERT INTO TEAM_STATUS VALUES ('T1006', 3, 1, 0, 2, 2, 5, 3);
INSERT INTO TEAM_STATUS VALUES ('T1007', 3, 0, 1, 2, 3, 5, 1);
INSERT INTO TEAM_STATUS VALUES ('T1008', 3, 1, 2, 0, 4, 3, 5);
INSERT INTO TEAM_STATUS VALUES ('T1009', 3, 1, 2, 0, 2, 1, 5);
INSERT INTO TEAM_STATUS VALUES ('T1010', 3, 1, 1, 1, 3, 3, 4);
INSERT INTO TEAM_STATUS VALUES ('T1011', 3, 0, 1, 2, 0, 2, 1);
INSERT INTO TEAM_STATUS VALUES ('T1012', 3, 2, 0, 1, 5, 1, 6);
INSERT INTO TEAM_STATUS VALUES ('T1013', 3, 1, 1, 1, 2, 2, 4);
INSERT INTO TEAM_STATUS VALUES ('T1014', 3, 1, 1, 1, 3, 6, 4);
INSERT INTO TEAM_STATUS VALUES ('T1015', 3, 1, 0, 2, 2, 3, 3);
INSERT INTO TEAM_STATUS VALUES ('T1016', 3, 3, 0, 0, 5, 1, 9);
INSERT INTO TEAM_STATUS VALUES ('T1017', 3, 2, 0, 1, 4, 2, 6);
INSERT INTO TEAM_STATUS VALUES ('T1018', 3, 1, 0, 2, 3, 6, 3);
INSERT INTO TEAM_STATUS VALUES ('T1019', 3, 0, 0, 3, 2, 5, 0);
INSERT INTO TEAM_STATUS VALUES ('T1020', 3, 1, 2, 0, 3, 1, 5);
INSERT INTO TEAM_STATUS VALUES ('T1021', 3, 1, 1, 1, 4, 5, 4);
INSERT INTO TEAM_STATUS VALUES ('T1022', 3, 0, 3, 0, 2, 2, 3);
INSERT INTO TEAM_STATUS VALUES ('T1023', 3, 0, 2, 1, 4, 5, 2);
INSERT INTO TEAM_STATUS VALUES ('T1024', 3, 2, 1, 0, 5, 2, 7);
INSERT INTO TEAM_STATUS VALUES ('T1025', 3, 1, 2, 0, 7, 0, 5);
INSERT INTO TEAM_STATUS VALUES ('T1026', 3, 1, 1, 1, 4, 3, 4);
INSERT INTO TEAM_STATUS VALUES ('T1027', 3, 0, 0, 3, 1, 12, 0);
INSERT INTO TEAM_STATUS VALUES ('T1028', 3, 2, 0, 1, 4, 2, 6);
INSERT INTO TEAM_STATUS VALUES ('T1029', 3, 2, 0, 1, 3, 2, 6);
INSERT INTO TEAM_STATUS VALUES ('T1030', 3, 1, 1, 1, 1, 1, 4);
INSERT INTO TEAM_STATUS VALUES ('T1031', 3, 0, 1, 2, 0, 3, 1);

UPDATE TEAM_STATUS SET no_matches_played = 0, no_wins = 0, no_draws = 0, no_losses = 0, no_goals_scored = 0, no_goals_received = 0, no_points = 0;

INSERT INTO MATCH_STATUS VALUES ('M1000', 46, 54, 2, 3, 0, 0, 0, 4);
INSERT INTO MATCH_STATUS VALUES ('M1001', 50, 50, 2, 0, 1, 0, 3, 4);
INSERT INTO MATCH_STATUS VALUES ('M1002', 59, 41, 2, 1, 0, 0, 6, 7);
INSERT INTO MATCH_STATUS VALUES ('M1003', 42, 58, 2, 2, 0, 0, 4, 5);
INSERT INTO MATCH_STATUS VALUES ('M1004', 53, 47, 2, 4, 0, 0, 7, 1);
INSERT INTO MATCH_STATUS VALUES ('M1005', 50, 50, 1, 0, 1, 0, 3, 5);
INSERT INTO MATCH_STATUS VALUES ('M1006', 58, 42, 1, 1, 0, 0, 10, 4);
INSERT INTO MATCH_STATUS VALUES ('M1007', 56, 44, 3, 2, 0, 0, 6, 2);
INSERT INTO MATCH_STATUS VALUES ('M1008', 33, 67, 1, 1, 0, 0, 1, 10);
INSERT INTO MATCH_STATUS VALUES ('M1009', 50, 50, 0, 1, 0, 0, 6, 11);
INSERT INTO MATCH_STATUS VALUES ('M1010', 45, 55, 3, 1, 0, 0, 1, 7);
INSERT INTO MATCH_STATUS VALUES ('M1011', 56, 44, 3, 1, 0, 1, 11, 3);
INSERT INTO MATCH_STATUS VALUES ('M1012', 53, 47, 3, 3, 0, 0, 8, 4);
INSERT INTO MATCH_STATUS VALUES ('M1013', 48, 52, 4, 1, 0, 0, 2, 4);
INSERT INTO MATCH_STATUS VALUES ('M1014', 51, 49, 2, 2, 0, 0, 4, 6);
INSERT INTO MATCH_STATUS VALUES ('M1015', 47, 53, 1, 1, 0, 0, 10, 3);
INSERT INTO MATCH_STATUS VALUES ('M1016', 45, 55, 3, 1, 0, 0, 2, 11);
INSERT INTO MATCH_STATUS VALUES ('M1017', 48, 52, 1, 2, 0, 0, 4, 3);
INSERT INTO MATCH_STATUS VALUES ('M1018', 55, 45, 2, 3, 0, 1, 4, 7);
INSERT INTO MATCH_STATUS VALUES ('M1019', 51, 49, 3, 4, 0, 0, 7, 1);
INSERT INTO MATCH_STATUS VALUES ('M1020', 45, 55, 1, 1, 0, 0, 4, 7);
INSERT INTO MATCH_STATUS VALUES ('M1021', 51, 49, 2, 2, 0, 0, 4, 4);
INSERT INTO MATCH_STATUS VALUES ('M1022', 50, 50, 3, 1, 0, 1, 6, 1);
INSERT INTO MATCH_STATUS VALUES ('M1023', 45, 55, 3, 2, 0, 0, 5, 6);
INSERT INTO MATCH_STATUS VALUES ('M1024', 58, 42, 2, 1, 0, 0, 6, 2);
INSERT INTO MATCH_STATUS VALUES ('M1025', 61, 39, 1, 0, 0, 0, 4, 5);
INSERT INTO MATCH_STATUS VALUES ('M1026', 51, 49, 2, 3, 0, 0, 6, 1);
INSERT INTO MATCH_STATUS VALUES ('M1027', 44, 56, 1, 1, 0, 0, 0, 3);
INSERT INTO MATCH_STATUS VALUES ('M1028', 56, 44, 3, 2, 0, 0, 7, 2);
INSERT INTO MATCH_STATUS VALUES ('M1029', 54, 46, 2, 2, 0, 0, 7, 2);
INSERT INTO MATCH_STATUS VALUES ('M1030', 52, 48, 1, 1, 0, 0, 8, 4);
INSERT INTO MATCH_STATUS VALUES ('M1031', 51, 49, 3, 1, 0, 0, 3, 1);
INSERT INTO MATCH_STATUS VALUES ('M1032', 57, 43, 2, 1, 0, 0, 2, 0);
INSERT INTO MATCH_STATUS VALUES ('M1033', 52, 48, 2, 1, 0, 0, 3, 10);
INSERT INTO MATCH_STATUS VALUES ('M1034', 50, 50, 4, 4, 0, 0, 6, 3);
INSERT INTO MATCH_STATUS VALUES ('M1035', 57, 43, 0, 3, 0, 0, 15, 0);
INSERT INTO MATCH_STATUS VALUES ('M1036', 63, 37, 1, 0, 0, 0, 7, 3);
INSERT INTO MATCH_STATUS VALUES ('M1037', 56, 44, 0, 3, 0, 0, 4, 3);
INSERT INTO MATCH_STATUS VALUES ('M1038', 39, 61, 4, 3, 0, 0, 4, 7);
INSERT INTO MATCH_STATUS VALUES ('M1039', 49, 51, 2, 1, 0, 0, 6, 4);
INSERT INTO MATCH_STATUS VALUES ('M1040', 54, 46, 2, 2, 0, 0, 5, 1);
INSERT INTO MATCH_STATUS VALUES ('M1041', 39, 61, 0, 0, 0, 0, 0, 7);
INSERT INTO MATCH_STATUS VALUES ('M1042', 63, 37, 0, 4, 0, 0, 12, 3);
INSERT INTO MATCH_STATUS VALUES ('M1043', 57, 43, 0, 2, 0, 0, 12, 2);
INSERT INTO MATCH_STATUS VALUES ('M1044', 41, 59, 2, 0, 0, 0, 3, 4);
INSERT INTO MATCH_STATUS VALUES ('M1045', 43, 57, 1, 2, 0, 0, 4, 6);
INSERT INTO MATCH_STATUS VALUES ('M1046', 58, 42, 6, 3, 0, 1, 5, 3);
INSERT INTO MATCH_STATUS VALUES ('M1047', 56, 44, 1, 4, 0, 0, 5, 3);

UPDATE MATCH_STATUS SET home_team_possession = 0, away_team_possession = 0, no_yellow_cards_home_team = 0, no_yellow_cards_away_team = 0, no_red_cards_home_team = 0, no_red_cards_away_team = 0, no_corners_home_team = 0, no_corners_away_team = 0;

INSERT INTO MATCHES VALUES ('M1000', 'C100', 'T1000', 'T1003', 'S1001', TO_DATE('03-06-2016 20:30','DD-MM-YYYY HH24:MI'), '0-0');
INSERT INTO MATCHES VALUES ('M1001', 'C100', 'T1002', 'T1000', 'S1005', TO_DATE('03-06-2016 20:30','DD-MM-YYYY HH24:MI'), '0-3');
INSERT INTO MATCHES VALUES ('M1002', 'C100', 'T1001', 'T1000', 'S1007', TO_DATE('03-06-2016 20:30','DD-MM-YYYY HH24:MI'), '0-1');
INSERT INTO MATCHES VALUES ('M1003', 'C100', 'T1002', 'T1001', 'S1000', TO_DATE('04-06-2016 16:00','DD-MM-YYYY HH24:MI'), '1-1');
INSERT INTO MATCHES VALUES ('M1004', 'C100', 'T1003', 'T1001', 'S1008', TO_DATE('04-06-2016 20:30','DD-MM-YYYY HH24:MI'), '0-2');
INSERT INTO MATCHES VALUES ('M1005', 'C100', 'T1003', 'T1002', 'S1006', TO_DATE('04-06-2016 16:00','DD-MM-YYYY HH24:MI'), '1-2');
INSERT INTO MATCHES VALUES ('M1006', 'C100', 'T1004', 'T1007', 'S1004', TO_DATE('05-06-2016 16:00','DD-MM-YYYY HH24:MI'), '1-0');
INSERT INTO MATCHES VALUES ('M1007', 'C100', 'T1004', 'T1005', 'S1000', TO_DATE('05-06-2016 13:30','DD-MM-YYYY HH24:MI'), '4-1');
INSERT INTO MATCHES VALUES ('M1008', 'C100', 'T1006', 'T1004', 'S1008', TO_DATE('05-06-2016 20:30','DD-MM-YYYY HH24:MI'), '0-2');
INSERT INTO MATCHES VALUES ('M1009', 'C100', 'T1005', 'T1006', 'S1003', TO_DATE('06-06-2016 13:30','DD-MM-YYYY HH24:MI'), '2-0');
INSERT INTO MATCHES VALUES ('M1010', 'C100', 'T1007', 'T1005', 'S1000', TO_DATE('06-06-2016 20:30','DD-MM-YYYY HH24:MI'), '2-2');
INSERT INTO MATCHES VALUES ('M1011', 'C100', 'T1006', 'T1007', 'S1006', TO_DATE('06-06-2016 16:00','DD-MM-YYYY HH24:MI'), '2-1');
INSERT INTO MATCHES VALUES ('M1012', 'C100', 'T1009', 'T1008', 'S1007', TO_DATE('07-06-2016 20:30','DD-MM-YYYY HH24:MI'), '1-1');
INSERT INTO MATCHES VALUES ('M1013', 'C100', 'T1010', 'T1008', 'S1004', TO_DATE('07-06-2016 16:00','DD-MM-YYYY HH24:MI'), '2-2');
INSERT INTO MATCHES VALUES ('M1014', 'C100', 'T1008', 'T1011', 'S1005', TO_DATE('07-06-2016 16:00','DD-MM-YYYY HH24:MI'), '1-0');
INSERT INTO MATCHES VALUES ('M1015', 'C100', 'T1009', 'T1011', 'S1000', TO_DATE('08-06-2016 20:30','DD-MM-YYYY HH24:MI'), '0-0');
INSERT INTO MATCHES VALUES ('M1016', 'C100', 'T1010', 'T1009', 'S1003', TO_DATE('08-06-2016 16:00','DD-MM-YYYY HH24:MI'), '0-1');
INSERT INTO MATCHES VALUES ('M1017', 'C100', 'T1011', 'T1010', 'S1008', TO_DATE('08-06-2016 13:30','DD-MM-YYYY HH24:MI'), '0-1');
INSERT INTO MATCHES VALUES ('M1018', 'C100', 'T1012', 'T1014', 'S1002', TO_DATE('09-06-2016 20:30','DD-MM-YYYY HH24:MI'), '4-0');
INSERT INTO MATCHES VALUES ('M1019', 'C100', 'T1012', 'T1015', 'S1003', TO_DATE('09-06-2016 13:30','DD-MM-YYYY HH24:MI'), '0-1');
INSERT INTO MATCHES VALUES ('M1020', 'C100', 'T1013', 'T1012', 'S1000', TO_DATE('09-06-2016 13:30','DD-MM-YYYY HH24:MI'), '0-1');
INSERT INTO MATCHES VALUES ('M1021', 'C100', 'T1015', 'T1013', 'S1005', TO_DATE('10-06-2016 16:00','DD-MM-YYYY HH24:MI'), '0-1');
INSERT INTO MATCHES VALUES ('M1022', 'C100', 'T1013', 'T1014', 'S1007', TO_DATE('10-06-2016 16:00','DD-MM-YYYY HH24:MI'), '1-1');
INSERT INTO MATCHES VALUES ('M1023', 'C100', 'T1014', 'T1015', 'S1009', TO_DATE('10-06-2016 23:30','DD-MM-YYYY HH24:MI'), '2-1');
INSERT INTO MATCHES VALUES ('M1024', 'C100', 'T1016', 'T1018', 'S1000', TO_DATE('11-06-2016 13:30','DD-MM-YYYY HH24:MI'), '2-0');
INSERT INTO MATCHES VALUES ('M1025', 'C100', 'T1016', 'T1017', 'S1002', TO_DATE('11-06-2016 13:30','DD-MM-YYYY HH24:MI'), '1-0');
INSERT INTO MATCHES VALUES ('M1026', 'C100', 'T1019', 'T1016', 'S1001', TO_DATE('11-06-2016 20:30','DD-MM-YYYY HH24:MI'), '1-2');
INSERT INTO MATCHES VALUES ('M1027', 'C100', 'T1017', 'T1019', 'S1006', TO_DATE('12-06-2016 16:00','DD-MM-YYYY HH24:MI'), '1-0');
INSERT INTO MATCHES VALUES ('M1028', 'C100', 'T1018', 'T1017', 'S1007', TO_DATE('12-06-2016 20:30','DD-MM-YYYY HH24:MI'), '1-3');
INSERT INTO MATCHES VALUES ('M1029', 'C100', 'T1019', 'T1018', 'S1005', TO_DATE('12-06-2016 20:30','DD-MM-YYYY HH24:MI'), '1-2');
INSERT INTO MATCHES VALUES ('M1030', 'C100', 'T1023', 'T1020', 'S1001', TO_DATE('13-06-2016 20:30','DD-MM-YYYY HH24:MI'), '1-1');
INSERT INTO MATCHES VALUES ('M1031', 'C100', 'T1021', 'T1020', 'S1006', TO_DATE('13-06-2016 13:30','DD-MM-YYYY HH24:MI'), '0-2');
INSERT INTO MATCHES VALUES ('M1032', 'C100', 'T1020', 'T1022', 'S1008', TO_DATE('13-06-2016 16:00','DD-MM-YYYY HH24:MI'), '0-0');
INSERT INTO MATCHES VALUES ('M1033', 'C100', 'T1022', 'T1021', 'S1007', TO_DATE('14-06-2016 13:00','DD-MM-YYYY HH24:MI'), '1-1');
INSERT INTO MATCHES VALUES ('M1034', 'C100', 'T1021', 'T1023', 'S1004', TO_DATE('14-06-2016 16:00','DD-MM-YYYY HH24:MI'), '3-2');
INSERT INTO MATCHES VALUES ('M1035', 'C100', 'T1023', 'T1022', 'S1009', TO_DATE('14-06-2016 16:00','DD-MM-YYYY HH24:MI'), '1-1');
INSERT INTO MATCHES VALUES ('M1036', 'C100', 'T1024', 'T1027', 'S1004', TO_DATE('15-06-2016 20:30','DD-MM-YYYY HH24:MI'), '2-1');
INSERT INTO MATCHES VALUES ('M1037', 'C100', 'T1024', 'T1026', 'S1000', TO_DATE('15-06-2016 20:30','DD-MM-YYYY HH24:MI'), '3-1');
INSERT INTO MATCHES VALUES ('M1038', 'C100', 'T1025', 'T1024', 'S1002', TO_DATE('15-06-2016 16:00','DD-MM-YYYY HH24:MI'), '0-0');
INSERT INTO MATCHES VALUES ('M1039', 'C100', 'T1026', 'T1025', 'S1003', TO_DATE('16-06-2016 16:00','DD-MM-YYYY HH24:MI'), '0-0');
INSERT INTO MATCHES VALUES ('M1040', 'C100', 'T1025', 'T1027', 'S1001', TO_DATE('16-06-2016 13:30','DD-MM-YYYY HH24:MI'), '7-0');
INSERT INTO MATCHES VALUES ('M1041', 'C100', 'T1027', 'T1026', 'S1009', TO_DATE('16-06-2016 16:00','DD-MM-YYYY HH24:MI'), '0-3');
INSERT INTO MATCHES VALUES ('M1042', 'C100', 'T1028', 'T1030', 'S1002', TO_DATE('17-06-2016 16:00','DD-MM-YYYY HH24:MI'), '0-1');
INSERT INTO MATCHES VALUES ('M1043', 'C100', 'T1028', 'T1031', 'S1004', TO_DATE('17-06-2016 20:30','DD-MM-YYYY HH24:MI'), '2-0');
INSERT INTO MATCHES VALUES ('M1044', 'C100', 'T1029', 'T1028', 'S1005', TO_DATE('17-06-2016 20:30','DD-MM-YYYY HH24:MI'), '1-2');
INSERT INTO MATCHES VALUES ('M1045', 'C100', 'T1031', 'T1029', 'S1009', TO_DATE('18-06-2016 13:30','DD-MM-YYYY HH24:MI'), '0-1');
INSERT INTO MATCHES VALUES ('M1046', 'C100', 'T1029', 'T1030', 'S1003', TO_DATE('18-06-2016 16:00','DD-MM-YYYY HH24:MI'), '1-0');
INSERT INTO MATCHES VALUES ('M1047', 'C100', 'T1030', 'T1031', 'S1006', TO_DATE('18-06-2016 20:30','DD-MM-YYYY HH24:MI'), '0-0');

UPDATE MATCHES SET score = null;

INSERT INTO TEAMS VALUES ('T1000', 'R1005', 'G1000', 'C1000', 'Uruguay', '4-4-2', 217630000, 8);
INSERT INTO TEAMS VALUES ('T1001', 'R1003', 'G1000', 'C1001', 'Mexico', '4-3-3', 96550000, 6);
INSERT INTO TEAMS VALUES ('T1002', 'R1000', 'G1000', 'C1002', 'South Africa', '3-4-3', 130560000, 4);
INSERT INTO TEAMS VALUES ('T1003', 'R1002', 'G1000', 'C1003', 'France', '4-2-3-1', 411750000, 6);
INSERT INTO TEAMS VALUES ('T1004', 'R1005', 'G1001', 'C1004', 'Argentina', '4-3-2-1', 391500000, 10);
INSERT INTO TEAMS VALUES ('T1005', 'R1001', 'G1001', 'C1005', 'Korea Republic', '4-4-2', 51930000, 4);
INSERT INTO TEAMS VALUES ('T1006', 'R1002', 'G1001', 'C1006', 'Greece', '4-3-3', 79900000, 5);
INSERT INTO TEAMS VALUES ('T1007', 'R1000', 'G1001', 'C1007', 'Nigeria', '4-2-3-1', 86850000, 4);
INSERT INTO TEAMS VALUES ('T1008', 'R1003', 'G1002', 'C1008', 'USA', '4-1-3-2', 185200000, 6);
INSERT INTO TEAMS VALUES ('T1009', 'R1002', 'G1002', 'C1009', 'England', '4-2-4', 334000000, 9);
INSERT INTO TEAMS VALUES ('T1010', 'R1002', 'G1002', 'C1010', 'Slovenia', '3-4-3', 83800000, 4);
INSERT INTO TEAMS VALUES ('T1011', 'R1000', 'G1002', 'C1011', 'Algeria', '4-5-1', 57200000, 6);
INSERT INTO TEAMS VALUES ('T1012', 'R1002', 'G1003', 'C1012', 'Germany', '4-3-2-1', 562000000, 10);
INSERT INTO TEAMS VALUES ('T1013', 'R1000', 'G1003', 'C1013', 'Ghana', '3-5-2', 93350000, 5);
INSERT INTO TEAMS VALUES ('T1014', 'R1004', 'G1003', 'C1014', 'Australia', '4-4-2', 21350000, 5);
INSERT INTO TEAMS VALUES ('T1015', 'R1002', 'G1003', 'C1015', 'Serbia', '3-4-3', 151650000, 4);
INSERT INTO TEAMS VALUES ('T1016', 'R1002', 'G1004', 'C1016', 'Netherlands', '4-3-2-1', 207560000, 7);
INSERT INTO TEAMS VALUES ('T1017', 'R1001', 'G1004', 'C1017', 'Japan', '4-2-3-1', 98000000, 4);
INSERT INTO TEAMS VALUES ('T1018', 'R1002', 'G1004', 'C1018', 'Denmark', '4-1-3-2', 106050000, 5);
INSERT INTO TEAMS VALUES ('T1019', 'R1000', 'G1004', 'C1019', 'Cameroon', '4-4-2', 118450000, 4);
INSERT INTO TEAMS VALUES ('T1020', 'R1005', 'G1005', 'C1020', 'Paraguay', '4-1-3-2', 63900000, 5);
INSERT INTO TEAMS VALUES ('T1021', 'R1002', 'G1005', 'C1021', 'Slovakia', '4-2-4', 92400000, 6);
INSERT INTO TEAMS VALUES ('T1022', 'R1004', 'G1005', 'C1022', 'New Zealand', '4-4-2', 7900000, 2);
INSERT INTO TEAMS VALUES ('T1023', 'R1002', 'G1005', 'C1023', 'Italy', '5-4-1', 323000000, 8);
INSERT INTO TEAMS VALUES ('T1024', 'R1005', 'G1006', 'C1024', 'Brazil', '3-4-3', 467500000, 9);
INSERT INTO TEAMS VALUES ('T1025', 'R1002', 'G1006', 'C1025', 'Portugal', '3-5-2', 297250000, 9);
INSERT INTO TEAMS VALUES ('T1026', 'R1000', 'G1006', 'C1026', 'Ivory Coast', '4-4-2', 127510000, 3);
INSERT INTO TEAMS VALUES ('T1027', 'R1001', 'G1006', 'C1027', 'Korea DPR', '4-4-2', 2330000, 3);
INSERT INTO TEAMS VALUES ('T1028', 'R1002', 'G1007', 'C1028', 'Spain', '3-6-1', 622000000, 10);
INSERT INTO TEAMS VALUES ('T1029', 'R1005', 'G1007', 'C1029', 'Chile', '3-5-2', 139300000, 9);
INSERT INTO TEAMS VALUES ('T1030', 'R1002', 'G1007', 'C1030', 'Switzerland', '4-3-3', 178000000, 8);
INSERT INTO TEAMS VALUES ('T1031', 'R1003', 'G1007', 'C1031', 'Honduras', '4-4-2', 21150000, 3);



ALTER TABLE Match_status ADD CONSTRAINT match_fk_Match_status FOREIGN KEY (ID_match) REFERENCES Matches(ID_match);
ALTER TABLE Goals ADD CONSTRAINT player_fk_Goals FOREIGN KEY (ID_player) REFERENCES Players(ID_player);
ALTER TABLE Goals ADD CONSTRAINT match_fk_Goals FOREIGN KEY (ID_match) REFERENCES Matches(ID_match);
ALTER TABLE Goals ADD CONSTRAINT team_fk_Goals FOREIGN KEY (ID_team) REFERENCES Teams(ID_team);
ALTER TABLE Matches ADD CONSTRAINT competition_fk_Matches FOREIGN KEY (ID_competition) REFERENCES Competitions(ID_competition);
ALTER TABLE Matches ADD CONSTRAINT home_team_fk_Matches FOREIGN KEY (ID_home_team) REFERENCES Teams(ID_team);
ALTER TABLE Matches ADD CONSTRAINT away_team_fk_Matches FOREIGN KEY (ID_away_team) REFERENCES Teams(ID_team);
ALTER TABLE Matches ADD CONSTRAINT stadium_fk_Matches FOREIGN KEY (ID_stadium) REFERENCES Stadiums(ID_stadium);
ALTER TABLE Players ADD CONSTRAINT team_fk_Players FOREIGN KEY (ID_team) REFERENCES Teams(ID_team);
ALTER TABLE Teams ADD CONSTRAINT region_fk_Teams FOREIGN KEY (ID_region) REFERENCES Regions(ID_region);
ALTER TABLE Teams ADD CONSTRAINT group_fk_Teams FOREIGN KEY (ID_group) REFERENCES Groups(ID_group);
ALTER TABLE Teams ADD CONSTRAINT coach_fk_Teams FOREIGN KEY (ID_coach) REFERENCES Coaches(ID_coach);
ALTER TABLE Competitions ADD CONSTRAINT region_fk_Competitions FOREIGN KEY (ID_region) REFERENCES Regions(ID_region);
ALTER TABLE Team_status ADD CONSTRAINT team_fk_Team_status FOREIGN KEY (ID_team) REFERENCES Teams(ID_team);
ALTER TABLE Coaches ADD CONSTRAINT team_fk_Coaches FOREIGN KEY (ID_team) REFERENCES Teams(ID_team);
ALTER TABLE Groups ADD CONSTRAINT competition_fk_Groups FOREIGN KEY (ID_competition) REFERENCES Competitions(ID_competition);



