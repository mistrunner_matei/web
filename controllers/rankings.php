<?php

class Rankings	extends Controller
{
	function __construct()
	{
		parent::__construct();
				Session::init();
		$logged = Session::get('loggedIn');

		if($logged == false){
			Session::destroy();
			header('location: ../Web/login');
			exit;
		}
	}

	function index()
	{
		$this->view->render("rankings/index");
	}

	function run_rankings()
	{
		$this->model->run_rankings();
	}
}