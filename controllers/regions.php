<?php

class Regions extends Controller
{
	function __construct()
	{
		parent::__construct();
		Session::init();
		$logged = Session::get('loggedIn');

		if($logged == false){
			Session::destroy();
			header('location: ../Web/login');
			exit;
		}
	}

	function index()
	{
		$this->view->render("regions/index");
	}

	function renderRegion($name)
	{
		$this->view->render("regions/list/" . $name);
	}
	
	function run_regions_africa(){
		
		$this->model->run_regions_africa();
	}
}