<?php

class Simulator extends Controller
{
	function __construct()
	{
		parent::__construct();
		$this->StartGame();
				Session::init();
		$logged = Session::get('loggedIn');

		if($logged == false){
			Session::destroy();
			header('location: ../Web/login');
			exit;
		}
	}

	function index()
	{
		$this->view->render("simulator/index");
	}

	function StartGame(){

	}

	public function GetRandom()
	{
		echo rand(1, 5);
	}

	public function back()
	{
		$this->model->insertData();
		header('location: ../index');
	}
}