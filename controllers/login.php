<?php

class Login extends Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->view->render("login/index");
	}

	function run_login()
	{
		$this->model->run_login();
	}

	function run_register()
	{
		$this->model->run_register();

	}
}