<?php

class Simulator_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function printFirstTeamName($string){
		$firstTeam = explode("-", $string);
		echo $firstTeam[0];
	}

	public function printSecondTeamName($string){
		$secondTeam = explode("-", $string);
		echo $secondTeam[1];
	}

	public function printFirstTeamCoach($string){
		
		$conn = parent::__construct();

		$teams = explode(" - ", $string);

		$team = $teams[0];

		$stid_user = oci_parse($conn,"SELECT c.first_name || ' ' || c.last_name FROM COACHES c JOIN TEAMS t ON c.id_team=t.id_team WHERE t.team_name ='$team'");
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result)
			{
				echo $result;
			}
		}
	}

	public function printSecondTeamCoach($string){
		
		$conn = parent::__construct();

		$teams = explode(" - ", $string);

		$team = $teams[1];

		$stid_user = oci_parse($conn,"SELECT c.first_name || ' ' || c.last_name FROM COACHES c JOIN TEAMS t ON c.id_team=t.id_team WHERE t.team_name ='$team'");
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result)
			{
				echo $result;
			}
		}
	}

	public function printFirstTeamPlayers($string){

		$conn = parent::__construct();

		$teams = explode(" - ", $string);

		$team = $teams[0];

		$stid_user = oci_parse($conn,"SELECT p.shirt_number || ' - ' || p.first_name || ' ' || p.last_name || ' - ' || p.pos FROM PLAYERS p JOIN TEAMS t on t.id_team = p.id_team WHERE t.team_name = '$team' AND p.lineup = 1 ORDER BY p.pos");
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result)
			{
				echo $result . "<br>";
			}
		}
	}

	public function printSecondTeamPlayers($string){

		$conn = parent::__construct();

		$teams = explode(" - ", $string);

		$team = $teams[1];

		$stid_user = oci_parse($conn,"SELECT p.pos || ' - ' || p.first_name || ' ' || p.last_name || ' - ' || p.shirt_number FROM PLAYERS p JOIN TEAMS t on t.id_team = p.id_team WHERE t.team_name = '$team' AND p.lineup = 1 ORDER BY p.pos");
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result)
			{
				echo $result . "<br>";
			}
		}
	}

	public function getFirstTeamScore($string){

		$conn = parent::__construct();

		$teams = explode(" - ", $string);

		$team1 = $teams[0];
		$team2 = $teams[1];

		$stid_user = oci_parse($conn,"BEGIN :val := chance_to_win(:team1, :team2); END;");
		oci_bind_by_name($stid_user,':val',$val,10000,SQLT_CHR);
		oci_bind_by_name($stid_user,':team1',$team1,10000,SQLT_CHR);
		oci_bind_by_name($stid_user,':team2',$team2,10000,SQLT_CHR);
		oci_execute($stid_user);

		$teams = explode("-", $val);

		$team1 = $teams[0];

		echo $team1;		
	}

	public function getSecondTeamScore($string){

		$conn = parent::__construct();

		$teams = explode(" - ", $string);

		$team1 = $teams[0];
		$team2 = $teams[1];

		$stid_user = oci_parse($conn,"BEGIN :val := chance_to_win(:team1, :team2); END;");
		oci_bind_by_name($stid_user,':val',$val,10000,SQLT_CHR);
		oci_bind_by_name($stid_user,':team1',$team1,10000,SQLT_CHR);
		oci_bind_by_name($stid_user,':team2',$team2,10000,SQLT_CHR);
		oci_execute($stid_user);

		$teams = explode("-", $val);

		$team1 = $teams[1];

		echo $team1;		
	}

	function getFirstMatch(){

		$conn = parent::__construct();

		$stid_user = oci_parse($conn,"SELECT t1.team_name || ' - ' || t2.team_name FROM TEAMS t1, TEAMS t2, MATCHES m WHERE t1.id_team = m.id_home_team AND m.id_away_team = t2.id_team AND m.start_time <= sysdate AND m.score is NULL ORDER BY m.start_time");
		
		oci_execute($stid_user);

		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS)){
			foreach ($row_user as $result_user) {
				return $result_user;
				}
			}
		} 

	function insertData(){
		$conn = parent::__construct();

		if(isset($_POST["submit"])) { 
			
			$doc = new DomDocument;

// We need to validate our document before refering to the id
$doc->validateOnParse = true;
$doc->Load('book.xml');

$res = $doc->getElementById('score')->nodeValue;

			$stid_user = oci_parse($conn,"UPDATE PLAYERS SET LAST_NAME = '$res' WHERE id_player = 'P1500'");
	
		oci_execute($stid_user);
	}
	}
}