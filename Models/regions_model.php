<?php

class Regions_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function run_regions_africa(){

			$result = "";
			$conn = parent::__construct();
			$regionA='R1000';
			$stid_region_africa = oci_parse($conn, "SELECT t.Team_name, t.Formation, t.Worth, t.No_stars, ts.No_matches_played, ts.No_wins, ts.No_draws, ts.No_losses, ts.No_goals_scored, ts.No_goals_received, ts.No_points FROM Teams t join Team_status ts on t.id_team = ts.id_team WHERE ID_region = '$regionA' ORDER BY No_points ASC, No_stars DESC");
			oci_execute($stid_region_africa);

			$result = "<table cellspacing="."0".">" ."<caption><h3>Africa</h3></caption>" . "<tr bgcolor="."#1f4e56".">" . "<th>" . "Name" . "</th><th>" . "Formation" . "</th><th>" . "Team worth" . "</th><th>" . "Stars" . "</th><th>" . "Matches played" . "</th><th>" . "Wins" . "</th><th>" . "Draws" . "</th><th>" . "Losses" . "</th><th>" . "Goals scored" . "</th><th>" . "Goals received" . "</th><th>" . "Points" . "</th>" . "</tr>";

			while($row_regionA = oci_fetch_array($stid_region_africa, OCI_ASSOC+OCI_RETURN_NULLS))
			{
				$result = $result . "<tr bgcolor="."#c1c1c1".">";
				foreach ($row_regionA as $item) 
				{
					$result = $result .  "<td>".($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp")."</td>\n";
				}
				$result = $result . "</tr>";
			}

			return $result;
	}
	
	public function run_regions_asia(){
		
		$result = "";
			$conn = parent::__construct();
			$regionA='R1001';
			$stid_region_africa = oci_parse($conn, "SELECT t.Team_name, t.Formation, t.Worth, t.No_stars, ts.No_matches_played, ts.No_wins, ts.No_draws, ts.No_losses, ts.No_goals_scored, ts.No_goals_received, ts.No_points FROM Teams t join Team_status ts on t.id_team = ts.id_team WHERE ID_region = '$regionA' ORDER BY No_points ASC, No_stars DESC");
			oci_execute($stid_region_africa);

			$result = "<table cellspacing="."0".">" ."<caption><h3>Asia</h3></caption>" . "<tr bgcolor="."#1f4e56".">" . "<th>" . "Name" . "</th><th>" . "Formation" . "</th><th>" . "Team worth" . "</th><th>" . "Stars" . "</th><th>" . "Matches played" . "</th><th>" . "Wins" . "</th><th>" . "Draws" . "</th><th>" . "Losses" . "</th><th>" . "Goals scored" . "</th><th>" . "Goals received" . "</th><th>" . "Points" . "</th>" . "</tr>";

			while($row_regionA = oci_fetch_array($stid_region_africa, OCI_ASSOC+OCI_RETURN_NULLS))
			{
				$result = $result . "<tr bgcolor="."#c1c1c1".">";
				foreach ($row_regionA as $item) 
				{
					$result = $result .  "<td>".($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp")."</td>\n";
				}
				$result = $result . "</tr>";
			}

			return $result;
	}
	
	public function run_regions_europa(){
		
			$result = "";
			$conn = parent::__construct();
			$regionA='R1002';
			$stid_region_africa = oci_parse($conn, "SELECT t.Team_name, t.Formation, t.Worth, t.No_stars, ts.No_matches_played, ts.No_wins, ts.No_draws, ts.No_losses, ts.No_goals_scored, ts.No_goals_received, ts.No_points FROM Teams t join Team_status ts on t.id_team = ts.id_team WHERE ID_region = '$regionA' ORDER BY No_points ASC, No_stars DESC");
			oci_execute($stid_region_africa);

			$result = "<table cellspacing="."0".">" ."<caption><h3>Europe</h3></caption>" . "<tr bgcolor="."#1f4e56".">" . "<th>" . "Name" . "</th><th>" . "Formation" . "</th><th>" . "Team worth" . "</th><th>" . "Stars" . "</th><th>" . "Matches played" . "</th><th>" . "Wins" . "</th><th>" . "Draws" . "</th><th>" . "Losses" . "</th><th>" . "Goals scored" . "</th><th>" . "Goals received" . "</th><th>" . "Points" . "</th>" . "</tr>";

			while($row_regionA = oci_fetch_array($stid_region_africa, OCI_ASSOC+OCI_RETURN_NULLS))
			{
				$result = $result ."<tr bgcolor="."#c1c1c1".">";
				foreach ($row_regionA as $item) 
				{
					$result = $result .  "<td>".($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp")."</td>\n";
				}
				$result = $result . "</tr>";
			}

			return $result;
	}



	public function run_regions_america(){
		$result = "";
			$conn = parent::__construct();
			$regionA='R1003';
			$regionB='R1005';
			
			$stid_region_africa = oci_parse($conn, "SELECT t.Team_name, t.Formation, t.Worth, t.No_stars, ts.No_matches_played, ts.No_wins, ts.No_draws, ts.No_losses, ts.No_goals_scored, ts.No_goals_received, ts.No_points FROM Teams t join Team_status ts on t.id_team = ts.id_team WHERE ID_region = '$regionA' OR ID_region='$regionB' ORDER BY No_points ASC, No_stars DESC");
			oci_execute($stid_region_africa);

		$result = "<table cellspacing="."0".">" ."<caption><h3>America</h3></caption>" . "<tr bgcolor="."#1f4e56".">" . "<th>" . "Name" . "</th><th>" . "Formation" . "</th><th>" . "Team worth" . "</th><th>" . "Stars" . "</th><th>" . "Matches played" . "</th><th>" . "Wins" . "</th><th>" . "Draws" . "</th><th>" . "Losses" . "</th><th>" . "Goals scored" . "</th><th>" . "Goals received" . "</th><th>" . "Points" . "</th>" . "</tr>";

			while($row_regionA = oci_fetch_array($stid_region_africa, OCI_ASSOC+OCI_RETURN_NULLS))
			{
				$result = $result . "<tr bgcolor="."#c1c1c1".">";
				foreach ($row_regionA as $item) 
				{
					$result = $result .  "<td>".($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp")."</td>\n";
				}
				$result = $result . "</tr>";
			}

			return $result;
	}



		
	public function run_regions_world(){
		
	$result = "";
			$conn = parent::__construct();
			
			$stid_region_africa = oci_parse($conn, "SELECT t.Team_name, t.Formation, t.Worth, t.No_stars, ts.No_matches_played, ts.No_wins, ts.No_draws, ts.No_losses, ts.No_goals_scored, ts.No_goals_received, ts.No_points FROM Teams t join Team_status ts on t.id_team = ts.id_team  ORDER BY No_points ASC, No_stars DESC");
			oci_execute($stid_region_africa);

			$result = "<table cellspacing="."0".">" ."<caption><h3>WorldWide</h3></caption>" . "<tr bgcolor="."#1f4e56".">" . "<th>" . "Name" . "</th><th>" . "Formation" . "</th><th>" . "Team worth" . "</th><th>" . "Stars" . "</th><th>" . "Matches played" . "</th><th>" . "Wins" . "</th><th>" . "Draws" . "</th><th>" . "Losses" . "</th><th>" . "Goals scored" . "</th><th>" . "Goals received" . "</th><th>" . "Points" . "</th>" . "</tr>";

			while($row_regionA = oci_fetch_array($stid_region_africa, OCI_ASSOC+OCI_RETURN_NULLS))
			{
				$result = $result ."<tr bgcolor="."#c1c1c1".">";
				foreach ($row_regionA as $item) 
				{
					$result = $result .  "<td>".($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp")."</td>\n";
				}
				$result = $result . "</tr>";
			}

			return $result;


	}
}
?>