<?php
class Search_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function run_search()
	{
		$conn = parent::__construct();

		$search = (isset($_POST['search']) ? $_POST['search'] : '');
		$search = strtoupper($search);

		$stid_user = oci_parse($conn,"SELECT COUNT(*) FROM TEAMS t JOIN TEAM_STATUS ts ON t.ID_team = ts.ID_team WHERE UPPER(team_name) LIKE '%$search%' OR UPPER(No_stars) LIKE '%$search%'"); 
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result)
			{
				if($result>0)
				{
					echo "<table cellspacing="."0".">" . "<caption><h3>Teams</h3></caption>" . "<tr bgcolor="."#1f4e56".">" . "<th>" . "Name" . "</th><th>" . "Formation" . "</th><th>" . "Team worth" . "</th><th>" . "Stars" . "</th><th>" . "Matches played" . "</th><th>" . "Wins" . "</th><th>" . "Draws" . "</th><th>" . "Losses" . "</th><th>" . "Goals scored" . "</th><th>" . "Goals received" . "</th><th>" . "Points" . "</th>" . "</tr>";
				}
			}
		}
		$stid_user = oci_parse($conn,"SELECT t.Team_name, t.Formation, t.Worth, t.No_stars, ts.No_matches_played, ts.No_wins, ts.No_draws, ts.No_losses, ts.No_goals_scored, ts.No_goals_received, ts.No_points FROM TEAMS t JOIN TEAM_STATUS ts ON t.ID_team = ts.ID_team WHERE UPPER(team_name) LIKE '%$search%' OR UPPER(No_stars) LIKE '%$search%'");
		oci_execute($stid_user);

		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			echo "<tr bgcolor="."#c1c1c1".">";
			$result_user = null;
			$i = 0;
			foreach ($row_user as $result_user[]) 
			{
				echo "<td>";
				echo $result_user[$i++];
				echo "</td>";
			}
			echo "</tr>";
		} 
		echo "</table>";

		$stid_user = oci_parse($conn,"SELECT COUNT(*) FROM Players WHERE UPPER(First_name) LIKE '%$search%' OR UPPER(Last_name) LIKE '%$search%' OR UPPER(Shirt_number) LIKE '%$search%' OR UPPER(Date_of_birth) LIKE '%$search%' OR UPPER(Nationality) LIKE '%$search%' OR UPPER(Pos) LIKE '%$search%'");   
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result)
			{
				if($result>0)
				{
					echo "<table cellspacing="."0".">" . "<caption><h3>Players</h3></caption>" . "<tr bgcolor="."#1f4e56".">" . "<th>" . "First name" . "</th><th>" . "Last name" . "</th><th>" . "Shirt number" . "</th><th>" . "Date of birth" . "</th><th>" . "Nationality" . "</th><th>" . "Pos" . "</th><th>" . "Lineup" . "</th>" . "</tr>";
				}
			}
		}
		$stid_user = oci_parse($conn,"SELECT First_name, Last_name, Shirt_number, Date_of_birth, Nationality, Pos, Lineup FROM Players WHERE UPPER(First_name) LIKE '%$search%' OR UPPER(Last_name) LIKE '%$search%' OR UPPER(Shirt_number) LIKE '%$search%' OR UPPER(Date_of_birth) LIKE '%$search%' OR UPPER(Nationality) LIKE '%$search%' OR UPPER(Pos) LIKE '%$search%'");
		oci_execute($stid_user);

		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			echo "<tr bgcolor="."#c1c1c1".">";
			$result_user = null;
			$i = 0;
			foreach ($row_user as $result_user[]) 
			{
				echo "<td>";
				echo $result_user[$i++];
				echo "</td>";
			}
			echo "</tr>";
		} 
		echo "</table>";

		$stid_user = oci_parse($conn,"SELECT COUNT(*) FROM Coaches WHERE UPPER(First_name) LIKE '%$search%' OR UPPER(Last_name) LIKE '%$search%' OR UPPER(Date_of_birth) LIKE '%$search%'");   
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result)
			{
				if($result>0)
				{
					echo "<table cellspacing="."0".">" . "<caption><h3>Coaches</h3></caption>" . "<tr bgcolor="."#1f4e56".">" . "<th>" . "First name" . "</th><th>" . "Last name" . "</th><th>" . "Date of birth" . "</th>" . "</tr>";
				}
			}
		}
		$stid_user = oci_parse($conn,"SELECT First_name, Last_name, Date_of_birth FROM Coaches WHERE UPPER(First_name) LIKE '%$search%' OR UPPER(Last_name) LIKE '%$search%' OR UPPER(Date_of_birth) LIKE '%$search%'");
		oci_execute($stid_user);

		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			echo "<tr bgcolor="."#c1c1c1".">";
			$result_user = null;
			$i = 0;
			foreach ($row_user as $result_user[]) 
			{
				echo "<td>";
				echo $result_user[$i++];
				echo "</td>";
			}
			echo "</tr>";
		} 
		echo "</table>";
	}
}