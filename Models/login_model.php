<?php

class Login_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function run_login()
	{
		$conn = parent::__construct();
		$user = (isset($_POST['login_username']) ? $_POST['login_username'] : '');
		$stid_user = oci_parse($conn, "SELECT COUNT(*) FROM users WHERE Username = '$user'");
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result_user) 
        	{
         		if($result_user == 1)
         		{
         			$pass = (isset($_POST['login_password']) ? $_POST['login_password'] : '');
         			$stid_pass = oci_parse($conn, "SELECT Encrypted_password FROM users WHERE Username = '$user'");
         			oci_execute($stid_pass);
         			while($row_pass = oci_fetch_array($stid_pass, OCI_ASSOC+OCI_RETURN_NULLS))
         			{
         				foreach ($row_pass as $result_pass)
         				{
         					if(password_verify($pass, $result_pass))
         					{
                           Session::init();
                           Session::set('loggedIn', true);
                           header('location: ../index');
         					}
         					else
         					{
         						//echo 'Incorrect username/password!';
                           header('location: ../login');
         					}
         				}
         			}
         		}
         		else
         		{
         			//echo 'Incorrect username/password!';
                  header('location: ../login');
         		}
        	}
		} 
	}

	public function run_register()
	{
		$conn = parent::__construct();
		$user = $_POST['register_username'];
		$stid_user = oci_parse($conn, "SELECT COUNT(*) FROM users WHERE Username = '$user'");
		oci_execute($stid_user);
		while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS))
		{
			foreach ($row_user as $result_user) 
        	{
         		if($result_user == 1)
         		{
         			return 'Username already exists!';
         		}
         		else
         		{
         			$pass = $_POST['register_password'];
         			$retyped_pass = $_POST['register_retyped_password'];
         			if($pass != $retyped_pass)
         			{
         				return "Passwords don't match!";
         			}
         			//to add password strength algorithm
         			else
         			{
         				$encrypted_pass = password_hash($pass, PASSWORD_DEFAULT);
         				$stid_register = oci_parse($conn, "INSERT INTO users VALUES('$user', '$encrypted_pass', 0, 0)");
         				oci_execute($stid_register);
         				 header('location: ../login');
         			}
         		}
         	}
         }
	}
}
?>