<?php

class Index_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

public function run_teams(){

	$conn = parent::__construct();

	$stid_user = oci_parse($conn,"SELECT COUNT(*) FROM TEAMS t1, TEAMS t2, MATCHES m WHERE t1.id_team = m.id_home_team AND m.id_away_team = t2.id_team AND m.start_time <= sysdate AND m.score is NULL ORDER BY m.start_time");

	oci_execute($stid_user);

	echo "<h4>Watch these matches</h4>";

	while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS)){
		foreach ($row_user as $result_user) {
			if($result_user == 0){
				echo "No more matches for now." . "<br>" . " Come back tomorrow!";
			}
			else{

				$stid_user = oci_parse($conn,"alter SESSION set NLS_DATE_FORMAT = 'DD-MM-YYYY HH24:MI'");

				oci_execute($stid_user);

				$stid_user = oci_parse($conn,"SELECT t1.team_name || ' - ' || t2.team_name, m.start_time FROM TEAMS t1, TEAMS t2, MATCHES m WHERE t1.id_team = m.id_home_team AND m.id_away_team = t2.id_team AND m.start_time <= sysdate AND m.score is NULL ORDER BY m.start_time");
		
				oci_execute($stid_user);

				echo "<table>";

				while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS)){

					echo "<tr>";
					$result_user = null;
					$i = 0;
					foreach ($row_user as $result_user[]) {
						echo "<td>";
						if($i == 0){
							echo "<a href= simulator?match=".urlencode($result_user[$i]).">" . $result_user[$i++] . "</a>";
						}
						else{
							echo $result_user[$i++];
						}
						echo "</td>";
					}
					echo "</tr>";
				} 
				echo "</table>";
			}
		}
	}

	$stid_user = oci_parse($conn,"SELECT COUNT(*) FROM TEAMS t1, TEAMS t2, MATCHES m WHERE t1.id_team = m.id_home_team AND m.id_away_team = t2.id_team AND m.start_time > sysdate AND m.start_time<=sysdate+1 ORDER BY m.start_time");
		
	oci_execute($stid_user);

	echo "<h4>Upcoming matches</h4>";

	while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS)){
		foreach ($row_user as $result_user) {
			if($result_user == 0){
				echo "This season is over.<br>Come back next year!";
			}
			else{
				$stid_user = oci_parse($conn,"SELECT t1.team_name || ' - ' || t2.team_name, m.start_time FROM TEAMS t1, TEAMS t2, MATCHES m WHERE t1.id_team = m.id_home_team AND m.id_away_team = t2.id_team AND m.start_time > sysdate AND m.start_time<=sysdate+1 ORDER BY m.start_time");
		
				oci_execute($stid_user);

				echo "<table id = \"upcoming\">";

				while($row_user = oci_fetch_array($stid_user, OCI_ASSOC+OCI_RETURN_NULLS)){
					echo "<tr>";
					$result_user = null;
					$i = 0;
					foreach ($row_user as $result_user[]) {
						echo "<td>";
						echo $result_user[$i++];
						echo "</td>";
					}
					echo "</tr>";
				}
				echo "</table>";
			}
		}
	}
		
}
} 
