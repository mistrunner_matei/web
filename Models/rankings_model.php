<?php

class Rankings_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function run_rankings()
	{
		$conn = parent::__construct();
	
		$countGroup = 0;	

		$group = array("GroupA","GroupB","GroupC","GroupD","GroupE","GroupF","GroupE","GroupG","GroupH");
		
		for($gr = 0; $gr < 9; $gr++)
		{

		$stid = oci_parse($conn, "SELECT t.Team_name,t.ID_team,t.ID_group  FROM teams t join Groups g on g.ID_group=t.ID_group where g.Group_name='$group[$gr]'");
		oci_execute($stid);
		while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) 
		{
		    foreach ($row as $grupa[$countGroup]) 
		    {
		      ($grupa[$countGroup] !== null ? htmlentities($grupa[$countGroup], ENT_QUOTES) : "&nbsp;");
		      $countGroup = $countGroup + 1;
		    }

		} 

		echo "<table cellspacing="."0"." width="."600".">";
		echo "<tr bgcolor="."#1f4e56".">"."<td>"."<div class=\"groupStatsName\">".$group[$gr]."</td><td>Matches</td><td>Wins</td><td>Draws</td><td>Losses</td><td>Goals-Scored</td><td>Goals-received</td><td>Points</td></tr></div>";
		for($count = 0; $count < $countGroup; $count++)
		{
		
			$stid = oci_parse($conn, "SELECT t.Team_name,s.No_matches_played,s.No_wins,s.No_draws,s.No_losses,s.No_goals_scored,s.No_goals_received,s.No_points  FROM teams t join Team_status s  on s.ID_team=t.ID_team where t.Team_name='$grupa[$count]'  order by s.No_points");
			oci_execute($stid);
			while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) 
			{
				if($count%2 == 0)
				{
			 	echo "<tr bgcolor="."#c1c1c1".">";
				    foreach ($row as $group_ordonare[$count]) 
				    {
				      ($group_ordonare[$count] !== null ? htmlentities($group_ordonare[$count], ENT_QUOTES) : "&nbsp;");
				      echo "<td>";
				      echo $group_ordonare[$count]."  ";
				      echo "</td>";
				    }
			    echo "</tr>";
			  	}
			  	else
			  	{
			 	echo "<tr bgcolor="."#a7a7a7".">";
				    foreach ($row as $group_ordonare[$count]) 
				    {
				      ($group_ordonare[$count] !== null ? htmlentities($group_ordonare[$count], ENT_QUOTES) : "&nbsp;");
				      echo "<td>";
				      echo $group_ordonare[$count]."  ";
				      echo "</td>";
				    }
			    echo "</tr>";
			  	}
			}
		}

		echo "</table>";
		$countGroup = 0;
		echo "<br>";
	}
	}//#1f4e56 verzui,#c1c1c1 light grey,#a7a7a7
}
?>