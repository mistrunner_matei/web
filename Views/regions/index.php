
<!DOCTYPE html>
<html>
<head>
	<title>Regions</title>
	<link rel="stylesheet" type="text/css" href="public/css/default.css">
	<link rel="stylesheet" type="text/css" href="public/css/regions.css">
</head>
<body>
	<div class="Main">
		<?php require 'views/header.php' ?>
		<div class="Content">
			<ul id = "regions_list">
				<li><a href ="regions/africa">Africa</a></li>
				<li><a href="regions/asia">Asia</a></li>
				<li><a href="regions/europe">Europe</a></li>
				<li><a href="regions/america">America</a></li>
				<li><a href="regions/worldwide">Worldwide</a></li>
			</ul>
		</div>
		<?php require 'views/footer.php' ?>
		

	</div>
</body>
</html>

