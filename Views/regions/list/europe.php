<!DOCTYPE html>
<html>
<head>
	<title>Europe</title>
	<link rel="stylesheet" type="text/css" href="../public/css/default.css" />
	<link rel="stylesheet" type="text/css" href="../public/css/regions_regions.css" />

</head>
<body>
	<div class = "Main">
		<?php require 'views/header.php' ?>
		<div class="Content">
			
				<div id ="africa" align="center">
					<?php $region = new Regions_Model(); ?>
			 		<?php echo $region->run_regions_europa(); ?>
				</div>
		</div>
	 

</div>

</body>
</html>






