<!DOCTYPE html>
<html>
<head>
	<title>Asia</title>
	<link rel="stylesheet" type="text/css" href="../public/css/default.css" />
	<link rel="stylesheet" type="text/css" href="../public/css/regions_regions.css" />

</head>
<body>
		<div class = "Main">
			<?php require 'views/header.php' ?>
				<div class="Content">
					<?php $region = new Regions_Model(); ?>
					<div 
						id ="africa" align="center"> <?php echo $region->run_regions_asia(); ?>
					</div>
				</div>
		</div>

</body>
</html>


