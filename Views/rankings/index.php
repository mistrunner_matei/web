 <!DOCTYPE html>
<html>
<head>
	<title>Rankings</title>
	<link rel="stylesheet" type="text/css" href="public/css/default.css">
	<link rel="stylesheet" type="text/css" href="public/css/rankings.css">
</head>
<body>
<div class = "Main">
	<?php require 'views/header.php' ?>
	<div class="Content">
		<div id="wrapper">
				<div id ="tables">
					<?php $t = new Rankings_Model(); ?>
					<?php echo $t->run_rankings(); ?>

				</div>
				<div id="menu">
					<hr>
					<a href="#">Fifa World Cup 2010</a>
					<hr>
				</div>
		</div>
	</div>


	<?php require 'views/footer.php' ?>

	</div>
</body>
</html>



