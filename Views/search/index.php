<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="public/css/default.css">
	<link rel="stylesheet" type="text/css" href="public/css/search.css">

</head>
<body>

<div class = "Main">
	<?php require 'views/header.php' ?>
	<div class="Content">
			<div id = "tables" align="center">
			 	<?php $result = new Search_Model(); ?>
				 <?php echo $result->run_search();?> 
			 </div>
	</div>
	<?php require 'views/footer.php' ?>

	</div>
		</body>	
</html>


