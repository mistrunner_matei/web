<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="public/css/default.css">
	<link rel="stylesheet" type="text/css" href="public/css/home.css">
	 	
</head>
<?php include "rows_Database.php" ?>
<body>
<div class = "Main">
	<?php require 'views/header.php' ?>
	<div class="Content">

		<div id="Middle">

			<div id="Video">

				<video width="400" height="300" controls>
				  <source src="WeBall.mp4" type="video/mp4">
				   <source src="WeBall.ogg" type="video/ogg">
				</video>

			</div>

			<div id = "Matches_to_watch">
				<?php $var = new Index_Model(); ?>
				<?php echo $var->run_teams(); ?>
			</div>

		</div>
	

		<div id="Bottom">

			<table id="leftTable" cellspacing="1">
				<tr>
					<td id="set1"></td>
				</tr>

				<tr>
					<td id="imgCountry"><img id="img1" src="" name="slide"></td>
				</tr>

			</table>
				
			<table id="rightTable" cellspacing="1">
				<tr>
					<td>Wins:</td><td id="set2"></td>
				</tr>

				<tr>
					<td>Draws:</td><td id="set3"></td>
				</tr>

				<tr>
					<td>Losses:</td><td id="set4"></td>
				</tr>

				<tr>
					<td>Goals:</td><td id="set5"></td>
				</tr>

				<tr>
					<td>Points:</td><td id="set6"></td>
				</tr>
			</table>
		</div>


	</div>
	<?php require 'views/footer.php' ?>

	</div>
		</body>
</html>


