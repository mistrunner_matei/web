currentMin = 0;

overTimeFirstHalf = getRandom(0,2);
overTimeSecondHalf = getRandom(1,4);

firstHalf = 1;

gameSpeed = 1500;

var firstHalft = 0;
var secondHalft = 0;

isPaused = false;
half = 1;
isInstant = false;

var team1Possesion = 50;
var team2Possesion = 50;

var currentTeam1Goals = 0;
var currentTeam2Goals = 0;
var currentTeam1YellowCards = 0;
var currentTeam2YellowCards = 0;
var currentTeam1RedCards = 0;
var currentTeam2RedCards = 0;
var currentTeam1Corners = 0;
var currentTeam2Corners = 0;
var currentTeam1Fouls = 0;
var currentTeam2Fouls = 0;

function randomEvent(randomNumber, team_1_score, team_2_score, arrayTeam1, arrayTeam2){

	var randomPlayer = 2 * getRandom(0, 5)  + 1;

	if(firstHalf == 1 && currentMin >45){
		var aux = "45+";
	}
	else if (firstHalf == 0 && currentMin >90){
		var aux = "90+";
	}
	else{
		var aux = currentMin;
	}
	if(randomNumber<team_1_score/100 * 350){
		currentTeam1Goals++;
		document.getElementById('eventTeam1').innerHTML += (aux + "'" + "<span style=\"color:green\">" + " Goal!!! " + "</span>" + arrayTeam1[randomPlayer] + "<br>");
		document.getElementById('score').innerHTML = currentTeam1Goals + ' - ' + currentTeam2Goals;
	}
	else if(randomNumber<350){
		currentTeam2Goals++;
		document.getElementById('eventTeam2').innerHTML += ( arrayTeam2[randomPlayer] + "<span style=\"color:green\">" + " Goal!!! " + "</span>" + aux + "'" + "<br>");
		document.getElementById('score').innerHTML = currentTeam1Goals + ' - ' + currentTeam2Goals;
	}
	else if(randomNumber<550){
		currentTeam1YellowCards++;
		document.getElementById('eventTeam1').innerHTML += (aux + "'" + "<span style=\"color:yellow\">" + " Yellow card " + "</span>" + arrayTeam1[randomPlayer] + "<br>");
	}
	else if(randomNumber<750){
		currentTeam2YellowCards++;
		document.getElementById('eventTeam2').innerHTML += ( arrayTeam2[randomPlayer] + "<span style=\"color:yellow\">" + " Yellow card " + "</span>" + aux + "'" + "<br>");
	}
	else if(randomNumber<800){
		currentTeam1RedCards++;
		document.getElementById('eventTeam1').innerHTML += (aux + "'" + "<span style=\"color:red\">" + " Red card " + "</span>" + arrayTeam1[randomPlayer] + "<br>");
	}
	else if(randomNumber<850){
		currentTeam2RedCards++;
		document.getElementById('eventTeam2').innerHTML += ( arrayTeam2[randomPlayer] + "<span style=\"color:red\">" + " Red card " + "</span>" + aux + "'" + "<br>");
	}
	else if(randomNumber<850 + (team_1_score/100 * 1100)){
		currentTeam1Corners++;
		document.getElementById('eventTeam1').innerHTML += (aux + "'" + "<span style=\"color:purple\">" + " Corner" + "</span>" + "<br>");
	}
	else if(randomNumber<1950){
		currentTeam2Corners++;
		document.getElementById('eventTeam2').innerHTML += ("<span style=\"color:purple\">" + " Corner " + "</span>" + aux + "'" + "<br>");
	}
	else if(randomNumber<1975){
		currentTeam2Goals++;
		document.getElementById('eventTeam1').innerHTML += (aux + "'" + "<span style=\"color:pink\">" + " Owngoal " + "</span>" + arrayTeam1[randomPlayer] + "<br>");
		document.getElementById('score').innerHTML = currentTeam1Goals + ' - ' + currentTeam2Goals;
	}
	else if(randomNumber<2000){
		currentTeam1Goals++;
		document.getElementById('eventTeam2').innerHTML += ( arrayTeam2[randomPlayer] + "<span style=\"color:pink\">" + " Owngoal " + "</span>" + aux + "'" + "<br>");
		document.getElementById('score').innerHTML = currentTeam1Goals + ' - ' + currentTeam2Goals;
	}
	else if(randomNumber<2700){
		currentTeam1Fouls++;
		document.getElementById('eventTeam1').innerHTML += (aux + "'" + "<span style=\"color:blue\">" + " Foul " + "</span>" + arrayTeam1[randomPlayer] + "<br>");
	}
	else if(randomNumber<3400){
		currentTeam2Fouls++;
		document.getElementById('eventTeam2').innerHTML += ( arrayTeam2[randomPlayer] + "<span style=\"color:blue\">" + " Foul " + "</span>" + aux + "'" + "<br>");
	}
	possesionVar = getRandom(1,2);
	if(possesionVar == 1){
		team1Possesion++;
		team2Possesion--;
	}
	else{
		team1Possesion--;
		team2Possesion++;
	}
	document.getElementById('possesion').innerHTML = team1Possesion + ' - ' + team2Possesion;
}

function firstHalfTimer() {
	currentMin++;

	document.getElementById('min').innerHTML = currentMin + '\'';

	for(var i = 1 ; i < 46 + overTimeFirstHalf; i++)
		if(currentMin == i){
			var randomNumber = getRandom(1, 10000);
			var team_1_score = Number(document.getElementById('firstTeamScore').innerHTML);
			var team_2_score = Number(document.getElementById('secondTeamScore').innerHTML);
			var team_1_players = document.getElementById('team1Players').innerHTML;
			var team_2_players = document.getElementById('team2Players').innerHTML;

			var arrayTeam1 = team_1_players.split(" - ");
			var arrayTeam2 = team_2_players.split(" - ");
			randomEvent(randomNumber, team_1_score, team_2_score, arrayTeam1, arrayTeam2);
		} 

		if(currentMin >= (45 + overTimeFirstHalf))
		{
			document.getElementById('secondHalfButton').style.display = 'inline';
			document.getElementById('controlls').style.textAlign  = 'center';
			currentMin = 45;
			pause();
			clearInterval(firstHalft);
		}
	}

	function secondHalfTimer(){
		currentMin++;

		document.getElementById('min').innerHTML = currentMin + '\'';

		document.getElementById('secondHalfButton').style.display = 'none';

		firstHalf = 0;

		for(var i = 46 ; i < 91 + overTimeSecondHalf; i++)
			if(currentMin == i){
				var randomNumber = getRandom(1, 10000);
				var team_1_score = Number(document.getElementById('firstTeamScore').innerHTML);
				var team_2_score = Number(document.getElementById('secondTeamScore').innerHTML);
				var team_1_players = document.getElementById('team1Players').innerHTML;
				var team_2_players = document.getElementById('team2Players').innerHTML;
				var arrayTeam1 = team_1_players.split(" - ");
				var arrayTeam2 = team_2_players.split(" - ");
				randomEvent(randomNumber, team_1_score, team_2_score, arrayTeam1, arrayTeam2);
			} 

			if(currentMin >= (90 + overTimeSecondHalf))
			{
				clearInterval(secondHalft);
				currentTeam1YellowCards;
				document.getElementById('YellowCardsT1').innerHTML = currentTeam1YellowCards;
				document.getElementById('YellowCardsT2').innerHTML = currentTeam2YellowCards;
				document.getElementById('RedCardsT1').innerHTML = currentTeam1RedCards;
				document.getElementById('RedCardsT2').innerHTML = currentTeam2RedCards;
				document.getElementById('FoultsT1').innerHTML = currentTeam1Fouls;
				document.getElementById('FoultsT2').innerHTML = currentTeam2Fouls;
				document.getElementById('CornersT1').innerHTML = currentTeam1Corners;
				document.getElementById('CornersT2').innerHTML = currentTeam2Corners;
			}
		}

		function startFirstHalf (){

			calltest();

			document.getElementById('firstHalfButton').style.display = 'none';	
			document.getElementById('secondHalfButton').style.display = 'none';

			firstHalft = setInterval(firstHalfTimer, gameSpeed);


		}

		function startSecondHalf(){
			document.getElementById('secondHalfButton').style.display = 'none';	

			secondHalft = setInterval(secondHalfTimer, gameSpeed);
		}

		function pause(){
			if(currentMin < 45 + overTimeFirstHalf){
				clearInterval(firstHalft);
				isPaused = true;
			}
			else if(currentMin >= 45 && currentMin < 90 + overTimeSecondHalf){
				clearInterval(secondHalft);
				isPaused = true;
			}
		}

		function play(){

			gameSpeed = 1500;

			if(currentMin < 45 ){
				clearInterval(firstHalft);
				firstHalft = setInterval(firstHalfTimer, gameSpeed);
				isPaused = false;
			}
			else if(currentMin >= 45 + overTimeFirstHalf && currentMin < 90 + overTimeSecondHalf){
				clearInterval(secondHalft);
				secondHalft = setInterval(secondHalfTimer, gameSpeed);
				isPaused = false;
			}
		}

		function speed2X(){
			gameSpeed = 1500 / 2;

			if(currentMin < 45 ){
				clearInterval(firstHalft);
				firstHalft = setInterval(firstHalfTimer, gameSpeed);
				isPaused = false;
			}
			else if(currentMin >= 45 + overTimeFirstHalf && currentMin < 90 + overTimeSecondHalf){
				clearInterval(secondHalft);
				secondHalft = setInterval(secondHalfTimer, gameSpeed);
				isPaused = false;
			}
		}

		function speed3X(){
			gameSpeed = 1500 / 15;

			if(currentMin < 45){
				clearInterval(firstHalft);
				firstHalft = setInterval(firstHalfTimer, gameSpeed);
				isPaused = false;
			}
			else if(currentMin >= 45 + overTimeFirstHalf && currentMin < 90 + overTimeSecondHalf){
				clearInterval(secondHalft);
				secondHalft = setInterval(secondHalfTimer, gameSpeed);
				isPaused = false;
			}
		}

		function instant(){
			gameSpeed = 0.1;

			if(currentMin < 45){
				clearInterval(firstHalft);
				firstHalft = setInterval(firstHalfTimer, gameSpeed);
				isPaused = false;
			}
			else if(currentMin >= 45 + overTimeFirstHalf && currentMin < 90 + overTimeSecondHalf){
				clearInterval(secondHalft);
				secondHalft = setInterval(secondHalfTimer, gameSpeed);
				isPaused = false;
			}
		}

		function getRandom(min, max){
			return (Math.floor(Math.random() * (max - min + 1)) + min);
		}


function set(php_array){
	js_array = php_array;
}
