<!DOCTYPE html>
<html>
<head>
	<title>Simulator</title>
	<script src="views/simulator/refresher.js"></script>
	<link rel="stylesheet" type="text/css" href="public/css/default.css">
	<link rel="stylesheet" type="text/css" href="public/css/simulator.css">
	<?php
	$db = new Simulator_Model();?>
</head>
<body>
	<div class="Main">
		<?php require 'views/header.php' ?>
		<div class="Content">
			<?php
			$var = new Simulator_Model(); 
			$string = isset($_GET['match']) ? $_GET['match'] : "";
			if($string == null){
				$string = $var->getFirstMatch();
			}
			?>
			<div id="field">
				<div id = "firstTeamDetails">
					<h2 id = "team1Name"> <?php 
						$var->printFirstTeamName($string);
						?>
					</h2>
					<h3 id = "firstTeamScore" > <?php
						$score = new Simulator_Model(); 
						$score->getFirstTeamScore($string);
						?> 
					</h3>
					<h4 id = "team1Coach">Coach: <?php
						$var->printFirstTeamCoach($string); ?></h4>
						<p id = "team1Players"> <?php $var->printFirstTeamPlayers($string); ?></p>
					</div>

					<img src = "<?php echo URL;?>Images/terrain.png" style = "height:379px;"></img>


					<div id = "secondTeamDetails">
						<h2 id = "team2Name"> <?php
							$var->printSecondTeamName($string); ?>
						</h2>
						<h3 id = "secondTeamScore"> <?php
							$score = new Simulator_Model(); 
							$score->getSecondTeamScore($string);
							?> 
						</h3>
						<h4 id = "team2Coach">Coach: <?php
							$var->printSecondTeamCoach($string); ?></h4>
							<p id = "team2Players"> <?php $var->printSecondTeamPlayers($string); ?></p>	
						</div>
					</div>


					<div id = "controlls">
						<div id= "buttons">
							<div id = "min"> 0' </div>
							<div id = "score"> 0 - 0 </div>
							<div id = "possessiontext">Possession</div>
							<div id = "possesion"> 50 - 50 </div>
							<button id = "pause" onclick="pause()"> || </button>
							<button id = "play" onclick="play()"> Play </button>
							<button id = "speed2X" onclick = "speed2X()"> 2X </button>
							<button id = "speed3X" onclick = "speed3X()"> 3X </button>
							<button id = "skip" onclick = "instant()"> Skip </button>
							<br>
							<button id = "secondHalfButton" onclick="startSecondHalf()" style="display:none"> Start Second Half.</button>

						</div>

						<div id= "eventTeam1"></div>
						<div id = "eventTeam2"></div>

						<div id = "Statistics">
							<div id = "StatisticsT1">
								<div id = "YellowCardsT1"></div>
								<div id = "FoultsT1"></div>
								<div id = "RedCardsT1"></div>
								<div id = "CornersT1"></div>
							</div>
							<div id = "Name">
								<div> Yellow cards </div>
								<div> Fouls </div>
								<div> Red cards </div>
								<div> Corners </div>
							</div>
							<div id = "StatisticsT2"> 
								<div id = "YellowCardsT2"></div>
								<div id = "FoultsT2"></div>
								<div id = "RedCardsT2"></div>
								<div id = "CornersT2"></div>
							</div>

						</div>
						<form action ="simulator/back" method="post">
						<input type="submit" name="submit" value="Finish Match">
						</form>

					</div>
				</div>
				<?php require 'views/footer.php' ?>
			</div>
		</body>
		</html>
