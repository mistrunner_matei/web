<?php

class Bootstrap
{
	function __construct()
	{
		$url = isset($_GET['url']) ? $_GET['url'] : null;


		$url = rtrim($url, '/');
		$url = explode('/', $url);

		if($url[0] == null)
		{
			require 'controllers/login.php';

			$controller = new Login();
			$controller->index();
			return false;
		}


		if((!file_exists('controllers/' . $url[0] . '.php')) && !file_exists('views/regions/list/' . $url[1] . '.php'))
		{
			require 'controllers/error.php';
			$controller = new Error();
			return false; 
		}

		$file = 'controllers/' . $url[0] . '.php';
		if(file_exists($file))
		{
			require $file;
		}
		else
		{
			require 'controllers/error.php';
			$controller = new Error();
			return false; 
		}

		$controller = new $url[0];
		$controller->loadModel($url[0]);

		if(isset($url[2]))
		{
			$controller->{$url[1]}($url[2]);
		}
		else
		{
			if(isset($url[1]))
			{

				if($url[0] == 'regions')
				{
					$controller = new Regions();
					$controller->renderRegion($url[1]);
				}
				else
					$controller->{$url[1]}();
			}
			else
			{
				$controller->index();

			}
		}

	}
}
